## GOPATH 工作模式的弊端
1、无版本控制概念
2、无法同步一致第三方版本号
3、无法指定当前项目引用的第三方版本号
## GO MOD命令
#### go mod 命令
go mod init         生成go.mod文件
go mod download     下载go.mod文件中知名的所有依赖
go mod tidy         整理现有的依赖
go mod graph        查看现有的依赖结构
go mod edit         编辑go.mod文件
go mod vendor       导出项目所有的依赖到vendor目录
go mod verify       检验一个模块是否被篡改过
go mod why          查看为什么需要依赖某模块
#### go mod 环境变量
go env         查看 
go env -w      修改

#### GO PROXY
主要用于设置go模块代理，作用是用于使go在后续拉取版本时直接通过镜像站点来快速拉去
默认值为：https://proxy.golang.org,directproxy.golang.org 国内访问不了，需要设置代理
阿里云：https://mirrors.aliyun.com/goproxy/
七牛云：https://goproxy.cn,direct
#### GOSUMDB
它的值是一个Go checksum database,用于在拉取模块版本时（无论是从源站拉取还是通过Go module proxy拉取)保证拉取到的模块版本数据未经过篡改，若发现不一致，也就是可能存在篡改，将会立即中止。
GOSUMDB的默认值为：sum.golang.org,在国内也是无法访问的，但是GOSUMDB可以被Go模块代理所代理（详见：Proxying a Checksum Database)。因此我们可以通过设置GOPROXY来解决，而先前我们所设置的模块代理goproxy.cn就能支持代理sum.golang.org,所以这一个问题在设置GOPROXY后，你可以不需要过度关心。
另外若对GOSUMDB的值有自定义需求，其支持如下格式：
·格式1：<SUMDB_NAME>+<PUBLIC_KEY>:。
·格式2：<SUMDB_NAME>+<PUBLIC_KEY><SUMDB_URL>,
也可以将其设置为“off”,也就是禁止G0在后续操作中校验模块版本。
#### GONOPROXY/GONOSUMDB/GOPRIVATE
这三个环境变量都是用在当前项目依赖了私有模块，例如像是你公司的私有gt仓库，又或是github中的私有库，都是属于私有模块，都是要进行设置的，否则会拉取失败。
更细致来讲，就是依赖了由GOPROXY指定的Go模块代理或由GOSUMDB指定Go checksum database都无法访问到的模块时的场景。
而一般建议直接设置GOPRIVATE,它的值将作为GONOPROXY和GONOSUMDB的默认值，所以建议的最佳姿势是直接使用GOPRIVATE。

并且它们的值都是一个以英文逗号“,”分割的模块路径前缀、也就是可以设置多个，例如：
```shell
go env -w GOPRIVATE="git.example.com,github.com/eddycjy/mquote"
```
设置后，前缀为git.xxx.com和github.com/eddycjy/mquote的模块都会被认为是私有模块。
如果不想每次都重新设置，我们也可以利用通配符，例如：
```shell
go env -w GOPRIVATE="*.example.com"
```
这样子设置的话，所有模块路径为example.com的子域名（例如：git.example.com)都将不经过Go module proxy和Go checksum database，需要注意的是不包括example.com本身。

#### 改变模块依赖关系
```shell
go mod edit -replace=xxx@xxx.xx=xxx@xx.xx
```