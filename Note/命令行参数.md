## 命令行参数

os.Args 是一个string的切片，用来存储所有的命令行参数

```go
package main

import (
    "fmt"
    "os"
)

//编写一段代码，可以获取命令行各个参数
func main() {
    fmt.Println("命令行的参数有", os.Args)
    //遍历os.Args 切片，就可以得到所有的命令行输入参数
    for i, arg := range os.Args {
        fmt.Printf("args[%v] = %v\n", i, arg)
    }
}
```

说明：前面的方式是比较原生的方式，对解析参数不是特别的方便，特别是带有指定
参数形式的命令行。
比如：`cmd>main.exe -f c:/aaa.txt -p 200 -u root`这样的形式命令行，go设计者给我们提供了flag包，可以方便的解析命令行参数，而且参数顺序可以随意

请编写一段代码，可以获取命令行各个参数.

```go

```
