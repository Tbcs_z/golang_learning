# Gouruntine和 chanel

引出：

需求：要求统计1~20000的数字中，哪些是素数？

思路：

    1、传统方法：使用循环，循环判断每一个数是否是素数。

    2、使用并发并行的方式，将统计素数的任务分配给多个goruntine去完成，这个时就会使用到goruntine【速度至少提高四倍】

#### 进程和线程说明

1）进程就是程序程序在操作系统中的一次执行过程，是系统进行资源分配和调度的基本单位
2）线程是进程的一个执行实例，是程序执行的最小单元，它是比进程更小的能独立运行的基本单位。
3）一个进程可以创建核销毁多个线程，同一个进程中的多个线程可以并发执行
4）一个程序至少有一个进程，一个进程至少有一个线程

#### 并发和并行

1）多线程程序在单核上运行，就是并发

2）多线程程序在多核上运行，就是并行

并发：因为是在一个cu上，比如有10个线程，每个线程执行10毫秒（进行轮询操作)，从人的角度看，好像这10个线程都在运行，但是从微观上看，在某一个时间点看，其实只有一个线程在执行，这就是并发。
并行：因为是在多个cpu上（比如有10个cpu),比如有10个线程，每个线程执行10毫秒（各自在不同cpu上执行），从人的角度看，这10个线程都在运行，但是从微观上看，在某一个时间点看，也同时有10个线程在执行，这就是并行。

#### GO协程和主线程

1、G0主线程（有程序员直接称为线程/也可以理解成进程）：一个G0线程上，可以起
多个协程，你可以这样理解，协程是轻量级的线程（编译器做的优化）。

2、Go协程的特点：有独立的栈空间；共享程序堆空间；调度由用户控制；协程是轻量级的线程

##### 快速入门

   案例：编写一个程序，完成如下功能

1、在主线程（可以理解成进程）中，开启一个goroutine,该协程每隔1秒输出"hello,world"
2、在主线程中也每隔一秒输出"hello,golang'",输出10次后，退出程序
3、要求主线程和goroutine同时执行，
4、画出主线程和协程执行流程图

```go

package main

import (
	"fmt"
	"strconv"
	"time"
)

//编写一个函数，每隔一秒输出 hello world
func test() {
	for i := 1; i <= 10; i++ {
		//strconv.Itoa
		fmt.Println("Hello world!" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}
func main() {
	go test() //开启一个协程
	for i := 1; i <= 10; i++ {
		//strconv.Itoa
		fmt.Println("Hello golang!" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}


```

![](C:\Users\15212\Documents\main\GOLANG\Note\img\uTools_1664433452983.png)

#### 小结

1）主线程是一个物理线程，直接作用在cpu上的。是重量级的，非常耗费cpu资源。
2）协程从主线程开启的，是轻量级的线程，是逻辑态。对资源消耗相对小。
3）Golang的协程机制是重要的特点，可以轻松的开启上万个协程。其它编程语言的并发机制是一般基于线程的，开启过多的线程，资源耗费大，这里就突显Golang在并发上的优势了

### MPG模式

1、M：操作系统的主线程（是物理线程）
2、P：协程执行需要的上下文
3、G：协程

##### MPG模式运行的状态1

1）当前程序有三个M，如果三个M都在一个cpu运行，就是并发，如果在不同的cpu运行就是并行
2）M1,M2,M3正在执行一个G，M1的协程队列有三个，M2的协程队列有3个，M3协程队列有2个
3）从上图可以看到：Go的协程是轻量级的线程，是逻辑态的，Go可以容易的起上万个协程。
4）其它程序c/java的多线程，往往是内核态的，比较重量级，几千个线程可能耗光cpu

##### MPG模式运行的状态2

1）分成两个部分来看
2）原来的情况是M0主线程正在执行G0协程，另外有三个协程在队列等待
3）如果G0协程阻塞，比如读取文件或者数据库等
4）这时就会创建M1主线程（也可能是从已有的线程池中取出M1),并且将等待的3个协程挂到M1下开始执行，M0的主线程下的G0仍然执行文件io的读写。
5）这样的MPG调度模式，可以既让G0执行，同时也不会让队列的其它协程一直阻塞，仍然
可以并发/并行执行。
6）等到G0不阻塞了，M0会被放到空闲的主线程继续执行（从已有的线程池中取），同时G0又会被唤醒。


###### 课后作业

1、创建一个Person结构体[Name,Age,Address]
2、使用rand方法配合随机创建10个Person实例，并放入到channeli中.
3、遍历channel,将各个Person实例的信息显示在终端

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Person struct {
	Name    string
	Age     int
	Address string
}

func main() {
	rand.Seed(time.Now().UnixNano())
	chaneli := make(chan Person, 10)
	for i := 0; i < 10; i++ {
		person := randP()
		chaneli <- person
		fmt.Println(person)
	}
	for i := 0; i < 10; i++ {
		a := <-chaneli
		fmt.Printf("%v :Name=%v,Age=%v,Address=%v\n", i+1, a.Name, a.Age, a.Address)

	}
}

func randP() Person {
	Name := RandomString(3)
	Address := RandomString(10)
	Age := rand.Intn(100)
	return Person{
		Name:    Name,
		Age:     Age,
		Address: Address,
	}
}

var defaultLetters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// RandomString returns a random string with a fixed length
func RandomString(n int, allowedChars ...[]rune) string {
	var letters []rune

	if len(allowedChars) == 0 {
		letters = defaultLetters
	} else {
		letters = allowedChars[0]
	}

	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}

```

### channel的遍历和关闭
###### chnnel的关闭
使用内置函数close可以关闭channel,.当channel关闭后，就不能再向channel写数据了，但是仍然可以从该channel读取数据
案例演示：
```go
package main

import "fmt"

func main() {
	intChan := make(chan int, 3)
	intChan <- 100
	intChan <- 200
	close(intChan) //close
	//此时不可再写入到channel
	//intChan <- 300
	fmt.Println(intChan)

}
```
###### channel的遍历
channel3支持for--range的方式进行遍历，请注意两个细节
1)在遍历时，如果channel设有关闭，则回出现deadlock的错误
2)在遍历时，如果channel i已经关闭，则会正常遍历数据，遍历完后，就会退出遍历。
```go
package main

import "fmt"

func main() {
	intChan := make(chan int, 3)
	intChan <- 100
	intChan <- 200
	close(intChan) //close
	//此时不可再写入到channel
	//关闭后可以读取数据
	n := <-intChan
	fmt.Println(n)

	//遍历管道
	intChan2 := make(chan int, 100)
	for i := 0; i < 100; i++ {
		intChan2 <- i * 2 // 放入100个数据到intChan2
	}
	//遍历：
	//for i := 0; i < len(intChan2); i++ {
	//} 这种方式只能取出50个，每取一次会减少长度
	//管道关闭后才可以遍历，如果遍历时没有关闭，则会出现deadlock
	close(intChan2)
	for v := range intChan2 {
		fmt.Println("v =", v)
	}
}
```

###### 请完成goroutine:和channeli协同工作的案例，具体要求：
1)开启一个writeData协程，向管道intChan中写入50个整数
2)开启一个readData协程，从管道intChan中读取writeData写入的数据。
3)注意：writeData和readData操作的是同一个管道
4)主线程需要等待writeData和readDate协程都完成工作才能退出
```go
package main

import "fmt"

// writeData
func writeData(intChan chan int) {
	for i := 1; i <= 50; i++ {
		//放入数据
		intChan <- i
		fmt.Println("write data:", i)
	}
	close(intChan)
}

// readData
func readData(intChan chan int, exitChan chan bool) {
	for {
		v, ok := <-intChan
		if !ok {
			break
		}
		fmt.Println("Read Data:", v)
	}
	//readData 读取完毕后，即任务完成，
	exitChan <- true
	close(exitChan)
}
func main() {
	//创建两个管道
	intChan := make(chan int, 50)
	exitChan := make(chan bool, 1)
	go writeData(intChan)
	go readData(intChan, exitChan)
	for {
		_, ok := <-exitChan
		if !ok {
			break
		}
	}
}
```
###### 作业：
1)启动一个协程，将1-2000的数放入到一个channel中，比如numChan
2)启动8个协程，从numChan:取出数（比如n),并计算1+.+n的值，并
存放到resChan
3)最后8个协程协同完成工作后，再遍历resChan,显示结果如res1=1.res10)=55
4)注意：考虑resChan chan int是否合适？
```go
package main

import "fmt"

func write(numChan chan int) {
	for i := 1; i <= 2000; i++ {
		numChan <- i
	}
	close(numChan)
}
func pip(numChan chan int, resChan chan int) {
	for {
		v, ok := <-numChan
		if ok {
			res := 0
			for i := 0; i <= v; i++ {
				res += i
			}
			resChan <- res
		} else {
			break
		}
	}
}
func main() {
	numChan := make(chan int, 2000)
	resChan := make(chan int, 2000)
	go write(numChan)
	for i := 0; i < 8; i++ {
		go pip(numChan, resChan)

	}
	for {
		//当长度达到2000则关闭管道
		if len(resChan) == 2000 {
			close(resChan)
			break
		}
	}
	num := 1
	for i := range resChan {
		fmt.Printf("resChan[%v] = %v\n", num, i)
		num++
	}
}
```