### string和slice

1、string底层是一个byte数组，因此string也可以进行切片操作

```go
    str := "helloGolang"
    //使用切片获取Golang
    slice := str[5:]
    fmt.Println("slice =", slice)
```

2、string切片在内存的形式以"abcd"画出内存示意图

![](C:\Users\15212\Documents\main\GOLANG\Note\img\QQ截图20220722132427.png)

3、string本身不可变，也就是说不能通过str[0]='z'的方式来改变字符串

4、如果需要修改字符串，可以先将string -> []byte / 或者[]rune ->修改>重写转成string

```go
//string本身不可变,如下方式会报错
    //str[o] = 'z'
    //可通过以下方式修改
    slice2 := []byte(str)
    //先转成一个切片，修改后再转成字符串
    slice2[0] = 'z'
    fmt.Println(slice2)
    str = string(slice2)
    fmt.Println(str)
    //细节：如果转成byte切片后，可以处理英文和数字，但是无法处理中文
    //原因：byte按一个字节处理，而一个汉字占3个字节，因此会出现乱码
    //解决方法：将string转成 []rune 即可,rune是按字符处理，兼容汉字
    slice3 := []rune(str)
    slice3[0] = '北'
    fmt.Println(slice3)
    str = string(slice3)
    fmt.Println(str)
```

### 切片练习

编写一个函数    fbn(n int)，要求完成：

    1、可以接收一个n int

    2、能够将斐波那契的数列放到切片中

    3、提示：斐波那契数列形式：

        arr[0] = 1,arr[1] = 1,arr[2] = 2,arr[3] = 3,arr[4] = 5,arr[5] = 8

```go
func fbn(n int) []uint64 {
    if n < 0 {
        fmt.Println("输入错误")
        return nil
    }
    arr := make([]uint64, n)
    if n == 0 {
        return arr
    } else if n == 1 {
        arr[0] = 1
        return arr
    }
    arr[0] = 1
    arr[1] = 1
    for i := 2; i < n; i++ {
        arr[i] = arr[i-1] + arr[i-2]
    }
    return arr
}
```
