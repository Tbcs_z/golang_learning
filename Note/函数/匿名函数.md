### 匿名函数

匿名函数使用方式1

在定义匿名函数时就直接调用这种方式匿名函数只能调用一次。

```go
package main
import{
    "fmt"
}
func main(){
    res1 := func(n1 int,n2 int) int{
        return n1 + n2
    }(10,20)
    fmt.Println("res1 =",res1)
}
```

匿名函数使用方式2
将匿名函数赋给一个变量（函数变量），再通过该变量来调用匿名函数

```go
package main
import{
    "fmt"
}
func main(){
    //res2的数据类型就是函数类型，此时，可以通过res2完成调用
    res2 := func(n1 int,n2 int) int{
        return n1 + n2
    }
    res := res2(10,30)
    fmt.Println("res =",res)
}
```

#### 全局匿名函数

```go
package main
import{
    "fmt"
}
var (
    //Fun1 就是一个全局匿名函数
    Fun1 = func(n1 int,n2 int)int{
         return n1*n2   
    }
)
func main(){
    //全局定义函数的使用
    res4 := Fun1(10,20)
    fmt.println("res4 = ",res4)
}
```
