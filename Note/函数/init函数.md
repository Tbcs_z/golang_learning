#### init函数

每一个源文件都可以包含一个init函数，该函数会在main函数执行前，被Go运行框架
调用，也就是说init会在main函数前被调用。

示例代码：

```go
package main

import {
    "fmt"
    "go_code/project01/main/function/utils"
}

var age = test()

//为了看到全局变量是先被初始化的，先写函数
func test() int {
    fmt.Println("test()") //1
    return 90
}

//init函数通常可以在init函数中完成初始化工作
func init() {
    fmt.Println("init()") //2
}
func main() {
    fmt.Println("main()...age = ", age) //3
    fmt.Println("Age = ",utils.Age,"Name = ",utils.Name)

}
```

**package utils**

```go
package utils

import "fmt"

var Age int
var Name string


func init(){
    Age = 100
    Name = "tom"
    fmt.Println("utils.init()...")
}
```

当两者都有变量定义及初始化函数时，引入执行顺序如下：

![](C:\Users\15212\AppData\Roaming\marktext\images\2022-07-11-14-03-40-image.png)
