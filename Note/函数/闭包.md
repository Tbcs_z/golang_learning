### 闭包

基本介绍：闭包就是一个函数和与其相关的引用环境组合的一个整体（实体）

```go
package main

import "fmt"

//累加器
func AddUpper() func(int) int {
	var n int = 10
	return func(x int) int {
		n = n + x
		return n
	}
}
func main() {
	f := AddUpper()
	fmt.Println("f(1) = ", f(1)) //11
	fmt.Println("f(2) = ", f(2)) //13
	fmt.Println("f(2) = ", f(3)) //16
}

```

代码说明：

    （1）AddUpper是一个函数，返回的类型是func(int) int

    （2）返回的是一个匿名函数，但是这个匿名函数引用到函数外的n，因此这个匿名函数就和n形成一个整体，构成闭包

    （3）可以这样理解：闭包是一个类，函数是操作，n是字段，函数和它使用到的n构成闭包。

    （4）当反复调用 f 函数时，n只初始化一次，因此每调用一次，就会进行累加。

    （5）分析出返回的函数使用到哪些变量，因为函数和它引用的变量共同构成闭包

#### 闭包的最佳实践

请编写一个程序，具体要求如下

1) 编写一个函数makeSuffix(suffix string)可以接收一个文件后缀名（比如.jpg),并返
   回一个闭包

2) 调用闭包，可以传入一个文件名，如果该文件名没有指定的后缀（比如.jpg),则返
   回文件名.jpg,如果已经有jpg后缀，则返回原文件名。

3) 要求使用闭包的方式完成

4) strings.HasSuffix，该函数可以判断某个字符串是否有指定的后缀

```go

package main

import (
	"fmt"
	"strings"
)

func makeSuffix(suffix string) func(str string) string {
	return func(str string) string {
		if strings.HasSuffix(str, suffix) == false {
			//如果str没有指定的后缀，则加上，否则就返回原来的名字
			return str + suffix
		}
		return str
	}
}
func main() {
	s := makeSuffix(".jpg")
	fmt.Println(s("xxx.jpg")) //xxx.jpg
	fmt.Println(s("you"))    //you.jpg
}


```




