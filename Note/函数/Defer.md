### Defer函数

在函数中，程序员经常需要创建资源（比如：数据库连接、文件句柄、锁等），为了在函数执行完毕后，及时的释放资源，Go的设计者提供defer(延时机制)。

```go

package main

import "fmt"

func main() {
	//写入defer关键字  end2先执行，类似于堆栈的方式，先入后出
	//defer fmt.Println("main end1")
	//defer fmt.Println("main end2")
	defer fun1()
	defer fun2()
	defer fun3()
	fmt.Println("main:hello go1")
	fmt.Println("main:hello go2")
}

func fun1() {
	fmt.Println("A")
}
func fun2() {
	fmt.Println("A")
}
func fun3() {
	fmt.Println("A")
}

```

示例2：

```go

package main

import "fmt"

func deferFunc() {
	fmt.Println("defer func called...")
}

func returnFunc() int {
	fmt.Println("return func called...")
	return 0
}
func returnanddefer() int {
	//defer在return之后执行，压栈
	defer deferFunc()
	return returnFunc()
}
func main() {
	returnanddefer()
}

```



defer细节注意事项

1、当go执行到一个defer时，不会立即执行defer后的语句，而是将defer后的语句压入到一个栈中，然后继续执行函数下一个语句。

2、当函数执行完毕后，在从defer栈中，一次从栈顶去除语句执行（注：遵守栈先入后出的机制）。

3、在defer语句放入到栈时，也会将相关的值拷贝同时入栈，示例代码：

```go
func sum(n1 int,n2 int)int{
    defer fmt.Println("OK1 n1 = ",n1)//10
    defer fmt.Println("OK2 n2 = ",n2)//20
    res := n1 + n2
    n1++
    fmt.Println("OK3 res = ",res,"n1 = ",n1)//res = 30 ,n1 = 11
    return res
}
func main(){
    sum(10,20)
}
```







1、在golang编程中的通常做法是，创建资源后，比如（打开了文件，获取了数据库的链接，或者是锁资源)，可以执行defer file.Close() defer connect.Close()
2、在defer后，可以继续使用创建资源
3、当函数完毕后，系统会依次从defer栈中，取出语句，关闭资源


