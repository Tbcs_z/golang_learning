## JSON

#### 概述：

JSON(JavaScript Object Notation)是一种轻量级的数据交换格式。易于人阅读和编写。同时也易于机器解析和生成。
JS0N是在2001年开始推广使用的数据格式，目前已经成为主流的数据格式。
JSON易于机器解析和生成，并有效地提升网络传输效率，通常程序在网络传输时会先将数据（结构体、map等)序列化son字符串，到接收方得到json字符串时，在反序列化恢复成原来哦的数据类型（结构体、map等）。这种方式已然成为各个语言的标准。

#### JSON数据格式说明

在S语言中，一切都是对象。因此，任何支持的类型都可以通过SON来表示，例如字符串、数字、对象、数组等

JSON键值对是用来保存数据一种方式，键/值对组合中的键名写在前面并用双引号"包裹，使用冒号：分隔，然后紧接着值：

如：` {"firstName":"Tom","address":"上海"}`

```go
package main

import (
    "encoding/json"
    "fmt"
)

//定义一个结构体
type Monster struct {
    Name     string  `json:"name"` //反射机制
    Age      int     `json:"age"`
    Birthday string  `json:"birthday"`
    Sal      float64 `json:"sal"`
    Skill    string  `json:"skill"`
}

func testStruct() {
    //演示对结构体的序列化操作
    monster := Monster{
        Name:     "牛魔王",
        Age:      500,
        Birthday: "1600.1.1",
        Sal:      4000,
        Skill:    "喷火",
    }
    //将monster 序列化
    data, err := json.Marshal(monster)
    if err != nil {
        fmt.Println("err:", err)
    }
    //输出序列化后的结果
    fmt.Println(string(data))
}
func testMap() {
    //定义一个mapo
    var a map[string]interface{}
    //使用map，需要make
    a = make(map[string]interface{})
    a["name"] = "红孩儿"
    a["age"] = 30
    a["address"] = "火焰山"
    data, err := json.Marshal(a)
    if err != nil {
        fmt.Println("err:", err)
    }
    //输出序列化后的结果
    fmt.Println(string(data))
}
func testSlice() {
    //定义一个
    var slice []map[string]interface{}
    var m1 map[string]interface{}
    //使用map，需要make
    m1 = make(map[string]interface{})
    m1["Name"] = "Jack"
    m1["Age"] = 19
    m1["Address"] = "四川"
    slice = append(slice, m1)
    var m2 map[string]interface{}
    m2 = make(map[string]interface{})
    m2["Name"] = "Tom"
    m2["Age"] = 29
    m2["Address"] = [2]string{"重庆", "墨西哥"}
    slice = append(slice, m2)
    data, err := json.Marshal(slice)
    if err != nil {
        fmt.Println("err:", err)
    }
    //输出序列化后的结果
    fmt.Println(string(data))
}

//对基本数据类型序列化
func testFloat64() {
    var num1 float64 = 2345.67
    data, err := json.Marshal(num1)
    if err != nil {
        fmt.Println("err:", err)
    }
    //输出序列化后的结果
    fmt.Println(string(data))
}
func main() {
    //演示将结构体，map，切片进行序列化
    testStruct()
    testMap()
    testSlice()
    testFloat64()
}
```

###### 注意，如果对于结构体，希望指定序列化后的名字，可以给struct指定一个tag标签

如：

```go
type Monster struct {
    Name     string  `json:"name"` //反射机制
    Age      int     `json:"age"`
    Birthday string  `json:"birthday"`
    Sal      float64 `json:"sal"`
    Skill    string  `json:"skill"`
}
```

## 反序列化

json反序列化是指，将json字符串反序列化成对应的数据类型（比如结构体、map、
切片）的操作。

```go
package main

import (
    "encoding/json"
    "fmt"
)

//反序列化
//定义一个结构体
type Monster struct {
    Name     string  `json:"name"` //反射机制
    Age      int     `json:"age"`
    Birthday string  `json:"birthday"`
    Sal      float64 `json:"sal"`
    Skill    string  `json:"skill"`
}

func testStruct() []byte {
    //演示对结构体的序列化操作
    monster := Monster{
        Name:     "牛魔王",
        Age:      500,
        Birthday: "1600.1.1",
        Sal:      4000,
        Skill:    "喷火",
    }
    //将monster 序列化
    data, err := json.Marshal(monster)
    if err != nil {
        fmt.Println("err:", err)
    }
    //输出序列化后的结果
    //fmt.Println(string(data))
    return data
}

//演示将json字符串，反序列化成struct
func unmarshallStruct() {
    //str在真实的项目开发中是通过网络传输获取的，或者是读取文件获取
    str := "{\"Name\":\"牛魔王\",\"Age\":500,\"Birthday\":\"2011-11-11\",\"Sal\":8000,\"Skill\":\"牛魔拳\"}"
    var data Monster
    err := json.Unmarshal([]byte(str), &data)
    if err != nil {
        fmt.Println("unmarshal fail,err:", err)
    }
    fmt.Println("反序列化后，Monster = ", data)
}
func UnM() {
    var monster Monster
    data := testStruct()
    err := json.Unmarshal([]byte(data), &monster)
    if err != nil {
        fmt.Println("unmarshal fail,err:", err)
    }
    fmt.Println(monster)
}
func unMarshalMap() {
    str := "{\"address\":\"火焰山\",\"age\":30,\"name\":\"红孩儿\"}"
    //定义一个map
    var data map[string]interface{}
    //注意：反序列化map时不需要make，因为make操作被封装到 Unmarshal函数里了
    //反序列化
    err := json.Unmarshal([]byte(str), &data)
    if err != nil {
        fmt.Println("unmarshal fail,err:", err)
    }
    fmt.Println(data)
}

//反序列化成slice
func unMarshalSlice() {
    str := "[{\"Address\":\"四川\",\"Age\"" +
        ":19,\"Name\":\"Jack\"},{\"" +
        "Address\":[\"重庆\",\"墨西哥\"],\"Age\":29,\"Name\":\"Tom\"}]\n"
    //定义一个切片
    var slice []map[string]interface{}
    err := json.Unmarshal([]byte(str), &slice)
    if err != nil {
        fmt.Println("unmarshal fail,err:", err)
    }
    fmt.Println(slice)
}
func main() {
    unmarshallStruct()
    UnM()
    unMarshalMap()
    unMarshalSlice()
}
```

#### 代码说明：

1、在反序列化一个json字符串时，要确保反序列化后的数据类型和原来序列化前的数据类型一致

2、如果json字符串是通过程序获取到的，则不需要再对双引号进行转义处理
