## Map

<b>基本语法：</b>

   ` var Mymap map[keytype]valuetype`

<b>keytype可以是什么类型：</b>

    golang中的map的key可以是很多种类型，比如bool,数字，string,指针，channel，还可以是只包含前面几个类型的接口，结构体，数组通常为int、string

注意：slice,map还有function不可以，因为这几个没法用==来判断

```go
package main

import "fmt"

func main() {
    /*
        map的三种创建方式
    */
    //--------------
    //第一种
    //声明myMap是一种map类型  map[key]value
    var myMap map[string]string
    if myMap == nil {
        fmt.Println("空map")
    }
    //在使用map前，需要先用make给map分配数据空间
    myMap = make(map[string]string, 10)
    myMap["one"] = "java"
    myMap["two"] = "C++"
    myMap["three"] = "Python"
    fmt.Println(myMap)

    //第二种声明方式
    myMap2 := make(map[int]string)
    myMap2[1] = "java"
    myMap2[2] = "C++"
    myMap2[3] = "Python"

    //第三种声明方式

    myMap3 := map[string]string{
        "one":   "java",
        "two":   "c++",
        "three": "python",
    }
    fmt.Println(myMap3)

    //声明map后，如果没有初始化，需要make为其开辟空间
}
```

提示：

1、map使用前一定要make开辟空间

2、map的key不能重复，如果重复，则以最后修改的为准

3、map的value可以相同

<img src="file:///C:/Users/15212/Documents/main/GOLANG/Note/img/uTools_1658731836569.png" title="" alt="" data-align="center">

课堂练习：演示一个key-vaue的value是map的案例
比如：我们要存放3个学生信息，每个学生有name和sex信息
思路：map[string]map[string]string

```go
    //课堂练习：演示一个key-vaue的value是map的案例
    //比如：我们要存放3个学生信息，每个学生有name和sex信息
    //思路：map[string]map[string]string
    Student := make(map[int]map[string]string)
    Student[1] = make(map[string]string, 2)
    Student[1]["name"] = "张三"
    Student[1]["sex"] = "男"
    Student[1]["province"] = "四川"
    Student[2] = make(map[string]string, 2) //这句话不能少
    Student[2]["name"] = "李四"
    Student[2]["sex"] = "女"
    Student[2]["province"] = "重庆"
    fmt.Println(Student[1])
    fmt.Println(Student[2])
    //遍历
    for i, m := range Student {
        fmt.Printf("k1 = %v\n", i)
        for s, s2 := range m {
            fmt.Printf("\t k2 = %v,v2 = %v\n", s, s2)
        }
    }
```

## Map的增删改查

增加：

  `  map[key] = value   如果还没有key就是更新，如果有则是更新`

```go
package main

import "fmt"

func printMap(cityMap map[string]string) {
    //cityMap 是一个引用传递
    for key, value := range cityMap {
        fmt.Println("key = ", key)
        fmt.Println("value = ", value)
    }
}

func ChangValue(cityMap map[string]string) {
    cityMap["England"] = "London"
}

func main() {
    cityMap := make(map[string]string)
    //添加
    cityMap["China"] = "BeiJing"
    cityMap["Japan"] = "Tokyo"
    cityMap["USA"] = "NewYork"
    //删除
    delete(cityMap, "China")
    //修改
    cityMap["USA"] = "DC"
    ChangValue(cityMap)
    //查找
    val, ok := cityMap["Japan"]
    if ok {
        fmt.Printf("有Japan，值为：%v\n", val)
    } else {
        fmt.Println("没有Japan")
    }
    //遍历
    printMap(cityMap)
    //map的长度
    fmt.Println("cityMap 有", len(cityMap), "对key-value")
}
```

说明：

    1、如果要删除map中的所有key，没有一个专门的方法一次性删除，可以遍历然后逐个删除

    2、或者 map = make(...)，make一个新的，让原来的成为垃圾，被gc回收

## Map 切片

切片的数据类型如果是map,则我们称为slice of map,map切片，这样使用则map个数就可以动态变化了。

```go
package main

import "fmt"

func main() {
    /*
        要求：使用一个map来记录monster的信息name和age,也就是说一个monster对应一个map,并
        且妖怪的个数可以动态的增加 => map切片
    */

    monster := make([]map[string]string, 2)
    //增加第一个信息
    if monster[0] == nil {
        monster[0] = make(map[string]string, 2)
        monster[0]["name"] = "牛魔王"
        monster[0]["age"] = "500岁"
    }
    if monster[1] == nil {
        monster[1] = make(map[string]string, 2)
        monster[1]["name"] = "玉兔精"
        monster[1]["age"] = "400岁"
    }
    //如下写法越界
    //if monster[2] == nil {
    //    monster[2] = make(map[string]string, 2)
    //    monster[2]["name"] = "狐狸精"
    //    monster[2]["age"] = "300岁"
    //}
    //需要使用到切片的append函数，可以动态增加
    newMonster := map[string]string{
        "name": "火云邪神",
        "age":  "1000岁",
    }
    monster = append(monster, newMonster)
    fmt.Println(monster)
}
```

## Map排序

1）golang中没有一个专门的方法针对map的key进行排序
2）golang中的map默认是无序的，注意也不是按照添加的顺序存放的，你每次遍
历，得到的输出可能不一样
3）golang中map的排序，是先将key进行排序，然后根据key值遍历输出即可

```go
package main

import (
    "fmt"
    "sort"
)

func main() {
    map1 := map[int]int{
        10: 100,
        1:  13,
        4:  36,
        8:  90,
    }
    fmt.Println(map1)
    //如果按照map的key的顺序进行排序输出
    //1、先将map的key放入到切片中
    //2、对切片排序
    //3、遍历切片，然后按照key来输出map的值

    var keys []int
    for i, _ := range map1 {
        keys = append(keys, i)
    }
    //排序
    sort.Ints(keys)
    fmt.Println(keys)
    for _, v := range keys {
        fmt.Printf("map[%v] = %v ", v, map1[v])
    }
}
```

在新版的golang（go1.18.1）中 ，已经支持自动排序，以上仅针对没有自动排序的老版本进行排序操作

## 使用细节和陷阱

1、map是引用类型，遵守引用类型传递的机制，在一个函数接收map,修改后，会直接修改原来的map。
2、map的容量达到后，再想map增加元素，会自动扩容，并不会发生panic，也就是说map能动态的增长键值对(key-value)

3、map的value也经常使用struct类型，更适合管理复杂的数据（比前面value是一个map更好），比如value为Student结构体

```go
package main

import (
    "fmt"
    "sort"
)

type Student struct {
    Name    string
    Sex     string
    Address string
}

func main() {
    //key为学号，value为结构体，包含姓名和性别
    stu := map[int]Student{
        1: {
            "张三",
            "男",
            "Chengdu",
        },
        2: {
            "李四",
            "女",
            "Wuhan",
        },
        3: {
            "Tom",
            "男",
            "England",
        },
    }
    fmt.Println(stu[1].Name)
    fmt.Println(stu[2].Name)
    //遍历各个学生的信息
    for i, student := range stu {
        fmt.Printf("学号：%v，姓名：%v，性别：%v，地址：%v\n", i, student.Name, student.Sex, student.Address)
    }
}
```

### 课堂习题

1、使用map[string]map[string]sting的map类型
2、key:表示用户名，是唯一的，不可以重复
3、如果某个用户名存在，就将其密码修改"888888"，如果不存在就增加这个用户
信息，（包括昵称nickname和密码pwd)
4、编写一个函数modifyUser(users map[string]map[string]string,name string),完
成上述功能

```go
package main

import "fmt"

func modifyUser(users map[string]map[string]string, name string) {
    if users[name] != nil {
        users[name]["password"] = "88888888"
    } else {
        //若没有则开辟一片空间，存储初始昵称和密码
        users[name] = make(map[string]string)
        users[name]["name"] = name + "nick"
        users[name]["password"] = "123456"
    }
}
func main() {
    user := map[string]map[string]string{
        "tom": {
            "nikName":  "Tompsion",
            "password": "123456",
        },
        "Jack": {
            "nikName":  "JackSmith",
            "password": "12345678",
        },
    }
    modifyUser(user, "welson")
    modifyUser(user, "tom")
    fmt.Println(user["tom"])
    fmt.Println(user["Jack"])
    fmt.Println(user["welson"])
}
```
