

### 使用Go Modules 初始化项目

（1）开启Go Modules

`go env -w GO111MODULE=on`

或者可以通过直接设置系统环境变量（写入对应的-/.bash_profile文件亦可）

`export GO111MODULE=on`

（2）初始化项目

`go mod init github.com/zxh/modules-test`





### 改变模块依赖关系

`go mod edit -replace=xxx@v.x.x.xxxxxx=xxx@v.x.x.xxxx`
