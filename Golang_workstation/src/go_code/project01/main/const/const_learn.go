package main

import "fmt"

const (
	a, b = iota + 1, iota + 2 //iota=0
	c, d                      //iota=1
	e, f                      //iota=2

	g, h = iota * 3, iota * 3 //iota=3
	i, j                      //iota=4
)

//const来定义枚举类型
const (
	//可以在const()添加一个关键字 iota，每行的iota都会累加1，第一行的iota默认值是0
	beijing  = 10 * iota //iota = 0
	shanghai             //iota =1
	shenzhen             //iota =2
)

func main() {
	//常量（只读）
	const length int = 10
	fmt.Println("length=", length)
	//length = 100 会报错，常量不允许修改
	fmt.Println("beijing = ", beijing)
	fmt.Println("shanghai = ", shanghai)
	fmt.Println("shenzhen = ", shenzhen)

	fmt.Println("a=", a, "b=", b)
	fmt.Println("c=", c, "d=", d)
	fmt.Println("e=", e, "f=", f)
	fmt.Println("g=", g, "h=", h)
	fmt.Println("i=", i, "j=", j)
}
