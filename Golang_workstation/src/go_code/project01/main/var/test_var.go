package main

import "fmt"

/*
	四种变量的声明方式
*/
func main() {
	//方法一：声明一个变量，默认的值是0
	var a int
	fmt.Println("a = ", a)
	fmt.Printf("type a = %T\n", a)
	//方法二：声明一个变量，初始化值
	var b int = 100
	fmt.Println("b = ", b)
	fmt.Printf("type b = %T\n", b)
	var bb = "abcd"
	fmt.Println("bb = ", bb)
	fmt.Printf("bb = %s ,type of bb = %T\n", bb, bb)
	//方法三：在初始化的时候，可以省去数据类型，通过值自动匹配当时的变量的数据类型
	var c = 100
	fmt.Println("c = ", c)
	fmt.Printf("type c = %T\n", c)

	var cc = "dsad"
	fmt.Println("cc = ", cc)
	fmt.Printf("cc = %s,type cc = %T\n", cc, cc)
	//方法4 （常用方法）省去var关键字，直接自动匹配
	//在声明全局变量时，方法一二三都可，但是方法4只能在函数体内声明
	e := 100
	fmt.Println("e = ", e)
	fmt.Printf("type of e = %T\n", e)
	f := 1.13
	fmt.Println("f = ", f)
	fmt.Printf("type of f = %T\n", f)
	g := "abcd"
	fmt.Println("g = ", g)
	fmt.Printf("type of g = %T\n", g)

	//声明多个变量
	var xx, yy int = 100, 100
	var zz, qq = "dsaa", 100
	fmt.Println("xx = ", xx, "yy = ", yy)
	fmt.Println("zz= ", zz, "qq = ", qq)
	//多行的多变量声明
	var (
		vv int  = 100
		jj bool = true
	)
	println("vv = ", vv, "jj = ", jj)
}
