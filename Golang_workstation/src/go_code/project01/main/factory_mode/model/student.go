package model

type student struct {
	Name string
	age  int
}

//通过工厂模式解决
func NewStudent(name string, age int) *student {
	return &student{
		Name: name,
		age:  age,
	}
}

//如果score字段首字母小写，则，在其他包不可以直接方法，可以提供一个方法
func (s student) GetAge() int {
	return s.age
}
