package main

import (
	"fmt"
	"go_code/project01/main/factory_mode/model"
)

func main() {
	//创建一个Student示例
	//var stu = model.NewStudent{
	//	Name: "Tom",
	//	Age:  78,
	//}
	//student结构体是小写，可以通过工厂模式解决
	var stu = model.NewStudent("Tom", 18)
	fmt.Println(*stu)
	fmt.Println("Name:", stu.Name, "Age:", stu.GetAge())
}
