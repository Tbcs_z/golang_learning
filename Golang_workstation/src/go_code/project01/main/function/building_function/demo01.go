package main

import "fmt"

func main() {
	number := 100
	fmt.Printf("Type = %T , 值为：%d，地址为 %v\n", number, number, &number)
	num2 := new(int) // *int
	*num2 = 100
	fmt.Printf("Type = %T , 值为：%v，地址为 %v,指向的值为：%v\n", num2, num2, &num2, *num2)
}
