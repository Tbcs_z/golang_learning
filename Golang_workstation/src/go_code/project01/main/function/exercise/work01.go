package main

import (
	"fmt"
)

//编写一个函数，从终端输入一个整数打印出对应的金字塔。
func printTower(level int) {
	for i := 1; i <= level; i++ {
		for k := 1; k <= level-i; k++ {
			fmt.Print(" ")
		}
		for x := 1; x <= 2*i-1; x++ {
			//空心金字塔
			//if x == 1 || x == 2*i-1 || i == level {
			//	fmt.Print("*")
			//} else {
			//	fmt.Print(" ")
			//}
			fmt.Print("*")
		}
		fmt.Println("")
	}
}

//九九乘法表
func chengfab() {
	for i := 1; i <= 9; i++ {
		for k := 1; k <= i; k++ {
			fmt.Print(k, "*", i, "=", i*k, "\t")
		}
		fmt.Println()
	}
}
func Reverse(arr *[3][3]int) {
	arr2 := [3][3]int{}
	for col, row := range arr {
		for j, _ := range row {
			arr2[col][j] = arr[j][col]
		}
	}
	for col, row := range arr {
		for j, _ := range row {
			arr[col][j] = arr2[col][j]
		}
	}
	Bianli(*arr)
}
func Bianli(arr [3][3]int) {
	for col, row := range arr {
		for j, _ := range row {
			x := arr[col][j]
			fmt.Printf("%v ", x)
		}
		fmt.Println()
	}
}
func main() {

	arr := [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	fmt.Println("转置前：")
	fmt.Println(arr)
	Bianli(arr)
	fmt.Println("转置后：")
	Reverse(&arr)
	fmt.Println(arr)
}
