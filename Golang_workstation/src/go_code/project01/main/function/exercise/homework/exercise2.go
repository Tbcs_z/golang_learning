package main

import (
	"fmt"
	"math/rand"
	"time"
)

/*
	2、生成一个1~100以内的随机数，有十次机会
	1次猜对提示 ”你真是个天才“
	2~3次猜对提示 ”你很聪明，赶上我了“
	4~9次猜对提示 ”一般般~“
	最后一次猜中，提示 ”可算猜对啦！“
	一次都没猜对则提示 ”说你点啥好呢“
*/

//生成1~100随机数
func rand100() int {
	//  设置种子，只需一次
	//  如果种子参数一样，每次运行程序产生的随机数都一样
	rand.Seed(time.Now().UnixNano()) //以当前系统时间作为种子参数
	//  产生随机数
	//fmt.Println("rand = ", rand.Int()) //  随机数很大
	//  限制在100以内
	return rand.Intn(100)
}
func NumberGame() {
	fmt.Println("------猜数字游戏------")
	time := 0
	num := rand100()
	fmt.Println(num)
	for {
		time++
		var guest int
		fmt.Print("请输入您猜的数字：")
		fmt.Scan(&guest)
		if guest == num || time > 10 {
			break
		} else if guest > num {
			fmt.Println("猜大了，请重新输入！")
		} else if guest < num {
			fmt.Println("猜小了，请重新输入！")
		}
	}
	switch time {
	case 1:
		fmt.Println("你真是个天才~")
	case 2, 3:
		fmt.Println("你很聪明，赶上我了~")
	case 4, 5, 6, 7, 8, 9:
		fmt.Println("一般般~")
	case 10:
		fmt.Println("可算猜对啦!")
	default:
		fmt.Println("说你点啥好呢？")
	}
}
func main() {
	NumberGame()
}
