package main

import "fmt"

func Calculator() {
	var choice int
	var num1 int
	var num2 int
	fmt.Println("-------------计算器-------------")
	fmt.Println("选择运算方式：")
	fmt.Println("\t1、加法\t2、减法\n\t3、乘法\t4、除法")
	fmt.Print("请选择：")
	fmt.Scan(&choice)
	fmt.Print("输入数字A：")
	fmt.Scan(&num1)
	fmt.Print("输入数字B：")
	fmt.Scan(&num2)
	switch choice {
	case 1:
		fmt.Println("结果为：", num1+num2)
	case 2:
		fmt.Println("结果为：", num1-num2)
	case 3:
		fmt.Println("结果为：", num1*num2)
	case 4:
		fmt.Println("结果为：", num1/num2)
	}
}
func main() {
	Calculator()
}
