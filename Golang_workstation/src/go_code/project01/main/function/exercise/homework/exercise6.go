package main

import "fmt"

/*
	输出小写的 a ~ z 以及大写的 Z ~ A
*/
func PrintWord() {
	var a []string
	var b []string
	//小写字符a到z的ASCII代码分别是097到122
	for i := 97; i <= 122; i++ {
		a = append(a, string(i))
	}
	fmt.Println()
	//大写字符A到Z的ASCII代码分别是065到090
	for i := 90; i >= 65; i-- {
		b = append(b, string(i))
	}
	fmt.Println(a)
	fmt.Println(b)
}
func main() {
	PrintWord()
}
