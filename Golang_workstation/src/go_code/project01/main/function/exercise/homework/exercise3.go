package main

import "fmt"

/*
	输出100以内所有素数，每行显示5个，并求和
*/
func isSu() {
	sum := 0
	enter := 0
	for i := 1; i <= 100; i++ {
		num := 2
		for x := 2; x < i; x++ {
			if i%x == 0 {
				break
			}
			num++
		}
		if i == num {
			sum += i
			enter++
			fmt.Print(i, "\t")
			if enter%5 == 0 {
				fmt.Println()
			}
		}
	}
	fmt.Println("\n100以内的素数和为：", sum)
}
func main() {
	isSu()
}
