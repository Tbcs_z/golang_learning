package main

import "fmt"

//  1、循环打印输入月份的天数（要求使用continue），同时需要判断输入的月份是否错误。

func Is31(month, year int) int {
	switch month {
	case 1, 3, 5, 7, 8, 10, 12:
		return 31
	case 4, 6, 9, 11:
		return 30
	case 2:
		if IsRun(year) {
			return 29
		} else {
			return 28
		}
	default:
		return 0
	}
}

func PrintDate() {
	for {
		var year int
		var month int
		var day int
		fmt.Print("\n请输入年份：")
		fmt.Scan(&year)
		if year < 1 {
			fmt.Println("输入错误，请重新输入")
			continue
		}
		fmt.Print("请输入月份：")
		fmt.Scan(&month)
		if month > 12 || month < 1 {
			fmt.Println("输入错误，请重新输入")
			continue
		}
		fmt.Print("请输入天数：")
		fmt.Scan(&day)
		if day > Is31(month, year) || day < 1 {
			fmt.Println("输入错误，请重新输入")
			continue
		}
		fmt.Printf("输入的日期为：%d年%d月%d日\n", year, month, day)
	}
}

func IsRun(year int) bool {
	if year%4 == 0 && year%100 != 0 || year%400 == 0 {
		return true
	} else {
		return false
	}
}
func main() {
	PrintDate()
}
