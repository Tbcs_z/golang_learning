package main

import (
	"fmt"
	"time"
)

/*
	古语有云：三天打鱼两天晒网
	如果从1990年1月1日开始执行三天打鱼两天晒网
	输入年月日判断这天是打鱼还是晒网
*/
func IsYuWang() {
	start := time.Date(1990, 1, 1, 0, 0, 0, 0, time.Local)
	year := 0
	mon := 0
	day := 0
	fmt.Print("输入年：")
	fmt.Scan(&year)
	fmt.Print("输入月：")
	fmt.Scan(&mon)
	fmt.Print("输入日：")
	fmt.Scan(&day)
	date := time.Date(year, time.Month(mon), day, 0, 0, 0, 0, time.Local)
	sub := int(date.Sub(start).Hours())
	//num := date.Unix() - start.Unix()
	if sub%120 < 72 {
		fmt.Println("今天打鱼")
	} else {
		fmt.Println("今天晒网")
	}
}
func main() {
	IsYuWang()
}
