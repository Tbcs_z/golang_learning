package main

import (
	"fmt"
	"strings"
)

var a int = 10

//累加器
func AddUpper() func(int) int {
	var n int = 10
	return func(x int) int {
		n = n + x
		return n
	}
}

func makeSuffix(suffix string) func(str string) string {
	return func(str string) string {
		if strings.HasSuffix(str, suffix) == false {
			//如果str没有指定的后缀，则加上，否则就返回原来的名字
			return str + suffix
		}
		return str
	}
}
func main() {
	f := AddUpper()
	fmt.Println("f(1) = ", f(1)) //11
	fmt.Println("f(2) = ", f(2)) //13
	fmt.Println("f(2) = ", f(3)) //16
	s := makeSuffix(".jpg")
	fmt.Println(s("xxx.jpg"))
	fmt.Println(s("you"))
	fmt.Println("全局变量：a = ", a)
	var a int = 20
	fmt.Println("局部变量：a = ", a)
}
