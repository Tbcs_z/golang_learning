package main

import (
	"errors"
	"fmt"
)

func CatchError() {
	err := recover() //recover 是个内置函数，可以捕获到异常
	if err != nil {  //说明捕获到异常或错误
		fmt.Println("msg:", err)
	}
}
func readConf(name string) (err error) {
	if name == "config.ini" {
		return nil
	} else {
		return errors.New("读取文件错误...")
	}
}
func test02() {
	err := readConf("config.ini")
	if err != nil {
		//如果读取文件发生错误，返回错误并终止程序
		panic(err)
	}
	fmt.Println("test02() continue.....")
}
func test() {
	defer CatchError()
	a := 10
	b := 0
	res := a / b
	fmt.Println(res)
	//使用defer + recover 的方式捕获异常
}

func main() {
	//test()
	//测试自定义错误使用
	test02()
	fmt.Println("main().....")
}
