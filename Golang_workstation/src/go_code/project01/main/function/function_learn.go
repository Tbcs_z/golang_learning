package main

import "fmt"

func main() {
	c := foo1("abc", 123)
	fmt.Println("c = ", c)

	d, e := foo2("abc", 999)
	fmt.Println("d = ", d, "e = ", e)

	ret1, ret2 := foo3("abc", 333)
	fmt.Println("ret1 = ", ret1, "ret2 = ", ret2)

	ret1, ret2 = foo4("abc", 333)
	fmt.Println("ret1 = ", ret1, "ret2 = ", ret2)
}
func foo1(a string, b int) int {
	fmt.Println("_____foo1____")
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	c := 100
	return c
}

//返回多个匿名返回值
func foo2(a string, b int) (int, int) {
	fmt.Println("_____foo2____")
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)

	return 666, 777
}

//返回多个有形参名的返回值
func foo3(a string, b int) (r1 int, r2 int) {
	fmt.Println("_____foo3____")
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)

	//r1，r2属于foo3的形参，初始化默认值为0
	//r1，r2的作用域是foo3整个函数
	fmt.Println("r1 = ", r1)
	fmt.Println("r2 = ", r2)

	//给有名称的返回值变量复制
	r1 = 100
	r2 = 200
	return
}

//返回多个有形参名的返回值
func foo4(a string, b int) (r1, r2 int) {
	fmt.Println("_____foo4____")
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	//给有名称的返回值变量复制
	r1 = 100
	r2 = 200
	return
}
