package main

/*
	导包的四种方式
	1、$GOPATH/xx/xx/包名
	2、_ $GOPATH/xx/xx/包名
	3、别名 $GOPATH/xx/xx/包名
	4、. $GOPATH/xx/xx/包名
*/

import (
	//加下划线表示匿名，无法使用当前包的方法，但是会执行当前的包内部的init()方法
	_ "go_code/project01/main/function/lib"
	mylib2 "go_code/project01/main/function/lib2"
	//表示调用当前包的全部方法，不建议使用
	//. "go_code/project01/main/function/lib2"
)

func main() {
	mylib2.Lib2Test()
}
