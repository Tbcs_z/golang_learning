package main

import (
	"fmt"
	"time"
)

func main() {
	//1、获取当前日期
	timeN := time.Now()
	fmt.Printf("time now is :%v,Type= %T\n", timeN, timeN)
	//2、获取其他日期
	fmt.Printf("年= %v ", timeN.Year())
	fmt.Printf("月= %v ", int(timeN.Month()))
	fmt.Printf("日= %v ", timeN.Day())
	fmt.Printf("时= %v ", timeN.Hour())
	fmt.Printf("分= %v ", timeN.Minute())
	fmt.Printf("秒= %v \n", timeN.Second())
	//3、格式化日期和时间
	//(1)第一种方式：使用Printf 或者 Springf
	fmt.Printf("time.Now = %v年%d月%v日%v时%v分%v秒\n", timeN.Year(), timeN.Month(), timeN.Day(),
		timeN.Hour(), timeN.Minute(), timeN.Second())
	date := fmt.Sprintf("time.Now = %v年%d月%v日%v时%v分%v秒", timeN.Year(), timeN.Month(), timeN.Day(),
		timeN.Hour(), timeN.Minute(), timeN.Second())
	fmt.Println(date)
	//(2)第二种方式 now.Format("2006/01/02/ 15:04:05")  format中的时间必须是固定的，否则会错误
	fmt.Printf(timeN.Format("2006/01/02/ 15:04:05"))
	fmt.Println()
	fmt.Printf(timeN.Format("2006年01月02日"))
	fmt.Println()
	fmt.Printf(timeN.Format("15:04:05"))

	//3、时间的常量
	//常量的应用：
	//（1）结合sleep使用
	//i := 0
	//for {
	//	i++
	//	fmt.Println(i)
	//	time.Sleep(time.Millisecond * 100) //休眠0.1秒
	//	if i == 100 {
	//		break
	//	}
	//}
	//获取当前unix时间戳  和  unixnano 时间戳  作用是可以获取随机数字
	fmt.Printf("unix 时间戳 = %v ,unixnano = %v\n", timeN.Unix(), timeN.UnixNano())
}
