package main

import (
	"fmt"
	"strconv"
	"time"
)

func test03() {
	str := ""
	for i := 0; i < 100000; i++ {
		str += "hello" + strconv.Itoa(i)
	}
}
func main() {
	before := time.Now().Unix()
	beforeNano := time.Now().UnixNano()
	test03()
	after := time.Now().Unix() - before
	afterNano := time.Now().UnixNano() - beforeNano
	fmt.Printf("test03函数运行时间= %v 秒\n", after)
	fmt.Printf("test03函数运行时间= %v 纳秒", afterNano)
}
