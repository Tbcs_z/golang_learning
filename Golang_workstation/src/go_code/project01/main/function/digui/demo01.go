package main

import "fmt"

func Sum(n int) int {
	if n < 1 || n > 10 {
		fmt.Printf("输入的天数错误！")
		return 0
	}
	if n == 10 {
		return 1
	} else {
		return (Sum(n+1) + 1) * 2
	}
}

func main() {
	fmt.Println("第一天的桃子数量是", Sum(1))
}
