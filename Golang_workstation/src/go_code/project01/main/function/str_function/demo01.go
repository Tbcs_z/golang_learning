package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	//len
	var str string = "abcd一"             //golang的编码统一为utf-8，ASCII码字母和数字占一个字节，汉字占三个字节
	fmt.Println("len(str) = ", len(str)) //7
	//
	str2 := "hello北京"
	str3 := []rune(str2)
	for i := 0; i < len(str3); i++ {
		fmt.Printf("字符 = %c \n", str3[i])
	}
	//strconv.Aoti(str)
	n, err := strconv.Atoi("123")
	if err != nil {
		fmt.Println("转换错误", err)
	} else {
		fmt.Println("转换结果：", n)
	}
	//strconv.Iota
	str = strconv.Itoa(1234)
	fmt.Printf("str = %v ,str = %T\n", str, str)
	//byte 切片
	var bytes = []byte("hello go")
	fmt.Printf("bytes = %c\n", bytes)

	str = string(bytes)
	fmt.Printf("str = %v,Type = %T\n", str, str)

	//十进制转
	str = strconv.FormatInt(123, 16)
	fmt.Println("123对应的16进制转换结果:", str)

	//查找子串
	fmt.Println(strings.Contains("seafood", "food")) //true
	//统计子串
	fmt.Println(strings.Count("ceheese", "e")) //4
	//不区分大小写比较
	fmt.Println(strings.EqualFold("abc", "Abc")) //true
	//返回子串在字符串第一次出现的index值，如果没有则返回 -1
	fmt.Println(strings.Index("cheese", "ee"))
	fmt.Println(strings.Index("cheese", "xx"))
	fmt.Println(strings.LastIndex("cheesee", "ee")) //5
	str = strings.Replace("go go hello", "go", "go 语言", 1)
	fmt.Println(str)

	strx := strings.Split("hello,world,ok", ",")
	fmt.Printf("str = %v,Type = %T\n", strx, strx)

	fmt.Println(strings.ToLower("Go")) //go
	fmt.Println(strings.ToUpper("Go")) //GO
	str = strings.TrimSpace(" hhhh xxxxx ")
	fmt.Printf("str = %q \n", str)

	str = strings.TrimLeft("! hello !", " !")
	fmt.Printf("str = %q \n", str)
	str = strings.TrimRight("! hello !", " !")
	fmt.Printf("str = %q \n", str)
	str = strings.Trim("! hello !", " !")
	fmt.Printf("str = %q \n", str)

	fmt.Println(strings.HasPrefix("ftp://192.168.1.1", "ftp")) //true
	fmt.Println(strings.HasSuffix("ftp://192.168.1.1", ".1"))  //true

}
