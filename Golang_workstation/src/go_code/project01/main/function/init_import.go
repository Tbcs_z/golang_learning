package main

import (
	"go_code/project01/main/function/lib"
	"go_code/project01/main/function/lib2"
)

func main() {
	lib.LibTest()
	lib2.Lib2Test()
}
