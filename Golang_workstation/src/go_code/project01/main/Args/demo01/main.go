package main

import (
	"fmt"
	"os"
)

//编写一段代码，可以获取命令行各个参数
func main() {
	fmt.Println("命令行的参数有", os.Args)
	//遍历os.Args 切片，就可以得到所有的命令行输入参数
	for i, arg := range os.Args {
		fmt.Printf("args[%v] = %v\n", i, arg)
	}
}
