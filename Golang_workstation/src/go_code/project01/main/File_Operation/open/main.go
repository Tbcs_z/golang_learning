package main

import (
	"fmt"
	"os"
)

func main() {

	//打开文件
	//概念说明：file的用法
	//1、file 叫 file 对象
	//2、file 叫 file 指针
	//3、file 叫 file 文件句柄
	file, err := os.Open("d:/test.txt")
	if err != nil {
		fmt.Println("open file err = ", err)
	}
	//输出文件
	fmt.Printf("file = %v \n", file)
	//关闭文件
	err = file.Close()
	if err != nil {
		fmt.Println("close file err :", err)
	}
}
