package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

// CharCount 统计一个文件中含有的英文、数字、空格及其他字符数量
/*
	思路：
	打开一个文件，创建一个Reader
	每读取一行，就去统计各字符数量
	然后将结果保存到一个结构体中
*/
//定义一个结构体，用于保存统计结果
type CharCount struct {
	chCount    int //英文
	NumCount   int //数字
	SpaceCount int //空格
	OtherCount int //其他
}

func main() {
	fileName := "E:/abc.txt"
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("err:", err)
		return
	}
	defer file.Close()
	var count CharCount
	//创建一个Reader
	reader := bufio.NewReader(file)
	//开始循环的读取文件内容
	for {
		str, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		//为了兼容中文字符，可以将str 转成 []rune
		//str = []rune(str)
		//遍历str
		for _, v := range str {
			switch {
			case v >= 'a' && v <= 'z':
				fallthrough //穿透
			case v >= 'A' && v <= 'Z':
				count.chCount++
			case v >= '0' && v <= '9':
				count.NumCount++
			case v == ' ' || v == '\t':
				count.SpaceCount++
			default:
				count.OtherCount++
			}
		}
	}
	fmt.Printf("英文的个数为：%v,数字的个数为：%v，空格的个数为：%v，其他字符的个数为：%v", count.chCount, count.NumCount, count.SpaceCount, count.OtherCount)
}
