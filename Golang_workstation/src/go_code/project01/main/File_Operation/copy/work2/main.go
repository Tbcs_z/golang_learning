package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

//拷贝 图片、压缩文件等

//自己编写一个函数，接受两个文件路径 srcFile dstFile
func CopyFile(srcFile, dstFile string) (written int64, err error) {
	src, err1 := os.Open(srcFile)
	if err1 != nil {
		fmt.Println("open file err:", err)
		return
	}
	defer src.Close()
	//通过srcFile获取到Reader
	reader := bufio.NewReader(src)
	//打开dstFIle
	dst, err2 := os.OpenFile(dstFile, os.O_RDONLY|os.O_WRONLY|os.O_CREATE, 0666)
	defer dst.Close()
	if err2 != nil {
		fmt.Println("open file err:", err)
		return
	}
	//通过dstFile 获取writer
	writer := bufio.NewWriter(dst)
	return io.Copy(writer, reader)

}
func main() {
	src := "D:/desktop/DSC_7315.jpg"
	dst := "E:/abc.jpg"
	_, err := CopyFile(src, dst)
	if err != nil {
		fmt.Println("err:", err)
	} else {
		fmt.Println("拷贝完成！")
	}
}
