package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

//编写一个函数，接受目标文件和源文件
func CpyFile(dstFileName string, srcFileName string) (written int64, err error) {
	srcFile, err := os.Open(srcFileName)
	if err != nil {
		fmt.Println("open file err:", err)
	}
	defer srcFile.Close()
	//通过srcFileName,获取到reader
	reader := bufio.NewReader(srcFile)
	//打开dstFileName
	dstFile, err := os.OpenFile(dstFileName, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("open file err:", err)
	}
	//通过dstFile，获取到writer
	writer := bufio.NewWriter(dstFile)
	writer.Flush()
	defer dstFile.Close()
	return io.Copy(writer, reader)
}
func main() {
	//将 D:/golangFiletest/abc.txt 拷贝到 E:/test.txt
	//调用copyFile 完成拷贝
	srcFile := "D:/golangFiletest/abc.txt"
	dstFile := "E:/test.txt"
	_, err := CpyFile(dstFile, srcFile)
	if err == nil {
		fmt.Println("拷贝完成")
	} else {
		fmt.Println("拷贝错误,err:", err)
	}
}
