package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	//编程一个程序，将一个文件的内容，写入到另外一个文件。注：这两个文件已经存在了
	filePath1 := "D:/test.txt"
	filePath2 := "D:/abc.txt"
	content, err := ioutil.ReadFile(filePath1)
	if err != nil {
		fmt.Println("read file err:", err)
	}
	err = ioutil.WriteFile(filePath2, content, 0666)
	if err != nil {
		fmt.Println("write file err :", err)
	}
}
