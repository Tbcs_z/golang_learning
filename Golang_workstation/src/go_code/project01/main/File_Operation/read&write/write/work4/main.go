package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	//4）打开一个存在的文件，将原来的内容读出显示在终端，并且追加5句"hello,北京！"
	filePath := "D:/test.txt"
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("open file err :", err)
		return
	}
	//先读取原来文件的内容，并在终端显示
	reader := bufio.NewReader(file)
	for {
		content, readerr := reader.ReadString('\n')
		if readerr == io.EOF {
			break
		}
		fmt.Print(content)
	}

	//准备写入5句
	str := "hello,北京\r\n" // \r 也表示换行
	//写入时，使用带缓存的”Writer"
	writer := bufio.NewWriter(file)
	for i := 0; i < 5; i++ {
		writer.WriteString(str)
	}
	//因为writer式带缓存的，因此在调用writeString方法时
	//其实内存式先写入到缓存的，所以需要调用flush方法，将缓冲的数据
	//真正写入到文件总，否则文件中会没有数据
	writer.Flush()
	//及时关闭file句柄，防止内存泄露
	defer file.Close()

}
