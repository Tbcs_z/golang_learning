package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	//3）打开一个存在的文件，在原来的内容追加内容‘ABC!ENGLISH!'
	filePath := "D:/test.txt"
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("open file err :", err)
		return
	}
	//准备写入10句
	str := "ABC!ENGLISH\r\n" // \r 也表示换行
	//写入时，使用带缓存的”Writer"
	writer := bufio.NewWriter(file)
	for i := 0; i < 10; i++ {
		writer.WriteString(str)
	}
	//因为writer式带缓存的，因此在调用writeString方法时
	//其实内存式先写入到缓存的，所以需要调用flush方法，将缓冲的数据
	//真正写入到文件总，否则文件中会没有数据
	writer.Flush()
	//及时关闭file句柄，防止内存泄露
	defer file.Close()

}
