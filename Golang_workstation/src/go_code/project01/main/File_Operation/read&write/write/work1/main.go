package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	//创建一个新文件，写入内容 5句 “Hello ,gardon"
	filePath := "D:/abc.txt"
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("open file err :", err)
		return
	}
	//准备写入5句
	str := "Hello ,gardon\n"
	//写入时，使用带缓存的”Writer"
	writer := bufio.NewWriter(file)
	for i := 0; i < 5; i++ {
		writer.WriteString(str)
	}
	//因为writer式带缓存的，因此在调用writeString方法时
	//其实内存式先写入到缓存的，所以需要调用flush方法，将缓冲的数据
	//真正写入到文件总，否则文件中会没有数据
	writer.Flush()
	//及时关闭file句柄，防止内存泄露
	defer file.Close()
}
