package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	//使用ioutil.ReadFile 一次性将文件读取到位
	file := "d:/test.txt"
	content, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println("read file err:", err)
	}
	//把读取到的内容显示到终端
	fmt.Printf("%v\n", content)       //[]byte
	fmt.Printf("%v", string(content)) //[]byte
	//因为没有显式的open文件，因此也不需要显式的close文件
	//因为文件的open和close被封装到 ReadFile 函数内部
}
