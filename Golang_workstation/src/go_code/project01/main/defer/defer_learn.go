package main

import "fmt"

func main() {
	//写入defer关键字  end2先执行，类似于堆栈的方式，先入后出
	//defer fmt.Println("main end1")
	//defer fmt.Println("main end2")
	defer fun1()
	defer fun2()
	defer fun3()
	fmt.Println("main:hello go1")
	fmt.Println("main:hello go2")
}

func fun1() {
	fmt.Println("A")
}
func fun2() {
	fmt.Println("A")
}
func fun3() {
	fmt.Println("A")
}
