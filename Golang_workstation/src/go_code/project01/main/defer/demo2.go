package main

import "fmt"

func deferFunc() {
	fmt.Println("defer func called...")
}

func returnFunc() int {
	fmt.Println("return func called...")
	return 0
}
func returnanddefer() int {
	//defer在return之后执行，压栈
	defer deferFunc()
	return returnFunc()
}
func main() {
	returnanddefer()
}
