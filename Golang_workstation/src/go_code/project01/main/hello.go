package main

import (
	"fmt"
	"time"
)

func main() { //函数的左括号一定是和函数名在同一行的，否则编译错误
	fmt.Println("Hello ,Go!")
	time.Sleep(1 * time.Second)
	//go run xxx.go
	//go build xxx.go
	//./xxx
}
