package main

import (
	"fmt"
	"strconv"
	"time"
)

/*
1、在主线程（可以理解成进程）中，开启一个goroutine,该协程每隔1秒输出"hello,world"
2、在主线程中也每隔一秒输出"hello,golang'",输出10次后，退出程序
3、要求主线程和goroutine同时执行
*/
//编写一个函数，每隔一秒输出 hello world
func test() {
	for i := 1; i <= 10; i++ {
		//strconv.Itoa
		fmt.Println("Hello world!" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}
func main() {
	x := 0
	fmt.Print("print a NUm:")
	fmt.Scan(&x)
	go test() //开启一个协程
	for i := 1; i <= x; i++ {
		//strconv.Itoa
		fmt.Println("Hello golang!" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}
