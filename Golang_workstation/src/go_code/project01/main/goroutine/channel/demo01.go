package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan int, 3) //带有缓冲的channel
	fmt.Println("len(c) = ", len(c), "cap(c) = ", cap(c))

	go func() {
		defer fmt.Println("go exited...")
		for i := 0; i < 4; i++ {
			c <- i
			fmt.Println("go程正在运行", "发送的元素=", i, "len(c)= ", len(c), "cap(c)", cap(c))
		}
	}()
	time.Sleep(2 * time.Second)
	for i := 0; i < 4; i++ {
		num := <-c //从c中接受并赋值给num
		fmt.Println("num = ", num)
	}
	fmt.Println("main exited")
	//当channel为空或者已满，从里面取数据也会阻塞
}
