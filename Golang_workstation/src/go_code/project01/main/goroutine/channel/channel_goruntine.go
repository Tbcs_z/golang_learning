package main

import "fmt"

//channel的基本定义与使用
func main() {
	//定义一个channel
	c := make(chan int)
	go func() {
		defer fmt.Println("goroutine exited")
		fmt.Println("goroutine is running...")
		c <- 666 //将666发送给c
	}()
	num := <-c //从c中接受数据，并赋值给c
	fmt.Println("num = ", num)
	fmt.Println("main goroutine exited...")
}
