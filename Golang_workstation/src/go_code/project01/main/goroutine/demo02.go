package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	//用go创建承载一个形参未空，返回值未空的一个函数
	go func() {
		defer fmt.Println("A.defer")

		func() {
			defer fmt.Println("B.defer")
			//推出当前gouroutine
			runtime.Goexit()
			fmt.Println("B")
		}()
		fmt.Println("A")
	}()

	go func(a int, b int) bool {
		fmt.Println("a = ", a, "b = ", b)
		return true
	}(10, 20)
	//死循环
	for {
		time.Sleep(1 * time.Second)
	}
}
