package main

import (
	"fmt"
	"go_code/project01/main/salary_system/object_method/Account"
)

//面向对象编程
func main() {
	Account.NewAccount().Menu()
	fmt.Println("已退出家庭记账软件！")
}
