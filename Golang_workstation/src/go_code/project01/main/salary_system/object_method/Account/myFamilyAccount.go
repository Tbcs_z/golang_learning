package Account

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Details struct {
	Choice  string
	Detail  string
	Balance float64
	Money   float64
	Reason  string
	Loop    bool
	Flag    bool
}

func NewAccount() *Details {
	return &Details{
		Choice:  "",
		Detail:  "",
		Balance: 10000.0,
		Money:   0.0,
		Reason:  "",
		Loop:    true,
		Flag:    true,
	}
}
func (detail *Details) Menu() {
	for detail.Loop {
		fmt.Println("\n---------家庭收支记账软件---------")
		fmt.Println("\t1、收支明细")
		fmt.Println("\t2、登记收入")
		fmt.Println("\t3、登记支出")
		fmt.Println("\t4、退    出")
		fmt.Print("\t请选择（1-4）：")
		fmt.Scanln(&detail.Choice)
		switch detail.Choice {
		case "1":
			detail.ShowDetail()
		case "2":
			detail.Income()
		case "3":
			detail.Expend()
		case "4":
			detail.Exit()
		default:
			fmt.Println("请输入正确的选项...")
		}
	}
}

//给结构体绑定相应的方法
func (detail *Details) ShowDetail() {
	fmt.Println("-----------当前收支明细-----------")
	fmt.Println("收支 \t 账户金额 \t 收支金额 \t 说明")
	if detail.Flag {
		fmt.Println("\t当前没有收支明细，来一笔吧~")
	}
	fmt.Printf("%v", detail.Detail)
}
func (detail *Details) Income() {
	fmt.Println("收入金额：")
	fmt.Scanln(&detail.Money)
	detail.Balance += detail.Money
	fmt.Println("收入说明：")
	//fmt.Scanln(&detail.Reason)
	reader := bufio.NewReader(os.Stdin)              // 标准输入输出
	detail.Reason, _ = reader.ReadString('\n')       // 回车结束
	detail.Reason = strings.TrimSpace(detail.Reason) // 去除最后一个空格
	//将收入情况，记录到detail中
	detail.Detail += fmt.Sprintf("收入 \t %v \t\t %v \t\t %v\n", detail.Balance, detail.Money, detail.Reason)
	detail.Flag = false
}
func (detail *Details) Expend() {
	fmt.Println("登记支出：")
	fmt.Scanln(&detail.Money)
	if detail.Money > detail.Balance {
		fmt.Println("余额不足！")
		return
	}
	detail.Balance -= detail.Money
	fmt.Println("支出说明：")
	//fmt.Scanln(&detail.Reason)
	//更换成bufio标准输入输出格式，避免空格结束输入
	reader := bufio.NewReader(os.Stdin)              // 标准输入输出
	detail.Reason, _ = reader.ReadString('\n')       // 回车结束
	detail.Reason = strings.TrimSpace(detail.Reason) // 去除最后一个空格
	//将收入情况，记录到detail中
	detail.Detail += fmt.Sprintf("支出 \t %v \t\t %v \t\t %v\n", detail.Balance, detail.Money, detail.Reason)
	detail.Flag = false
}
func (detail *Details) Exit() {
	confirm := ""
	exit := true
	for exit {
		fmt.Print("是否确认退出（y/n）：")
		fmt.Scanln(confirm)
		switch confirm {
		case "y", "Y":
			detail.Loop = false
			exit = false
			break
		case "n", "N":
			exit = false
			break
		default:
			fmt.Println("输入错误，请重新输入！")
		}
	}
}
