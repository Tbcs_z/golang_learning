package main

import "fmt"

//面向过程编程
func main() {
	//接受用户选择的变量
	choice := ""
	loop := true
	//定义收支明细变量
	var Detail string
	var Balance float64 = 10000
	var Money float64
	var Reason string
	for loop {
		fmt.Println("\n---------家庭收支记账软件---------")
		fmt.Println("\t1、收支明细")
		fmt.Println("\t2、登记收入")
		fmt.Println("\t3、登记支出")
		fmt.Println("\t4、退    出")
		fmt.Print("\t请选择（1-4）：")
		fmt.Scanln(&choice)
		switch choice {
		case "1":
			fmt.Println("-----------当前收支明细-----------")
			fmt.Println("收支 \t 账户金额 \t 收支金额 \t 说明")
			if Detail == "" {
				fmt.Println("\t当前没有收支明细，来一笔吧~")
			}
			fmt.Printf("%v", Detail)
		case "2":
			fmt.Println("收入金额：")
			fmt.Scanln(&Money)
			Balance += Money
			fmt.Println("收入说明：")
			fmt.Scanln(&Reason)
			//将收入情况，记录到detail中
			Detail += fmt.Sprintf("收入 \t %v \t\t %v \t\t %v\n", Balance, Money, Reason)
		case "3":
			fmt.Println("登记支出：")
			fmt.Scanln(&Money)
			if Money > Balance {
				fmt.Println("余额不足！")
				break
			}
			Balance -= Money
			fmt.Println("支出说明：")
			fmt.Scanln(&Reason)
			//将收入情况，记录到detail中
			Detail += fmt.Sprintf("支出 \t %v \t\t %v \t\t %v\n", Balance, Money, Reason)
		case "4":
			confirm := ""
			exit := true
			for exit {
				fmt.Print("是否确认退出（y/n）：")
				fmt.Scanln(&confirm)
				switch confirm {
				case "y", "Y":
					loop = false
					exit = false
					break
				case "n", "N":
					exit = false
					break
				default:
					fmt.Println("输入错误，请重新输入！")
				}
			}
		default:
			fmt.Println("请输入正确的选项...")

		}
	}
	fmt.Println("已退出家庭记账软件！")

}
