package main

import "fmt"

type MethodUtils struct {
}

func (m *MethodUtils) Table(n int) {
	if n < 1 || n > 9 {
		fmt.Println("输入的参数错误~！")
		return
	}
	for i := 1; i <= n; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf("%d * %d = %d\t", j, i, i*j)
		}
		fmt.Println()
	}
}
func (m MethodUtils) Reverse(arr *[3][3]int) {
	fmt.Println("转置前：")
	for _, i2 := range arr {
		for _, i4 := range i2 {
			fmt.Print(i4, " ")
		}
		fmt.Println()
	}
	temp := *arr
	for i, _ := range arr {
		for j := 0; j < 3; j++ {
			arr[j][i] = temp[i][j]
		}
	}
	fmt.Println("转置后：")
	for _, i2 := range arr {
		for _, i4 := range i2 {
			fmt.Print(i4, " ")
		}
		fmt.Println()
	}
}
func main() {
	var m MethodUtils
	m.Table(10)
	arr := [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	m.Reverse(&arr)
}
