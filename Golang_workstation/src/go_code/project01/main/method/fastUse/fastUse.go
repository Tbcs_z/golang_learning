package main

import "fmt"

type Person struct {
	Name string
}
type Student struct {
	Name string
	Age  int
}

func (stu *Student) String() string {
	str := fmt.Sprintf("Name = [%v],Age = [%v]", stu.Name, stu.Age)
	return str
}

//（1）给Person结构体添加speak方法，输出xxx是一个好人
func (person Person) speak() {
	fmt.Println(person.Name, "是一个好人")
}

//（2）给Persons结构体添加jisuan方法，可以计算从1+.+1000的结果
func (person Person) jisuan() {
	sum := 0
	for i := 1; i <= 1000; i++ {
		sum += i
	}
	fmt.Println("1+....+1000 = ", sum)
}

//（3）给Person:结构体jisuan2方法，该方法可以接收一个数n,计算从1+.+n的结果
func (person Person) jisuan2(n int) {
	sum := 0
	for i := 0; i <= n; i++ {
		sum += i
	}
	fmt.Printf("1+....+%v = %v\n", n, sum)
}

//（4）给Person:结构体添加getSum方法，可以计算两个数的和，并返回结果。
func (person Person) getSum(a, b int) int {
	return a + b
}
func main() {
	person := Person{Name: "张飞"}
	person.speak()
	person.jisuan()
	fmt.Println(person.getSum(1, 2))
	person.jisuan2(10)

	stu := Student{
		Name: "Tom",
		Age:  17,
	}
	fmt.Println(stu)
	fmt.Println(&stu)
}
