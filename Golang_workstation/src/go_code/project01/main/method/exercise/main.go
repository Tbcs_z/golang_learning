package main

import "fmt"

type MethodUtils struct {
}

func (receiver MethodUtils) Print10() {
	for i := 0; i < 10; i++ {
		for j := 0; j < 8; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}
func (receiver MethodUtils) PrintMn(m, n int) {
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}
func (receiver MethodUtils) Area(len, width float64) float64 {
	return width * len
}
func (receiver *MethodUtils) test4(n int) {
	if n%2 != 0 {
		fmt.Println(n, " 是奇数")
	} else {
		fmt.Println(n, " 是偶数")
	}
}
func (receiver *MethodUtils) test5(m, n int, char string) {
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			fmt.Print(char)
		}
		fmt.Println()
	}
}

type Calculator struct {
	Num1 float64
	Num2 float64
}

func (receiver *Calculator) test6(char string) float64 {
	res := 0.0
	switch char {
	case "+":
		res = receiver.Num1 + receiver.Num2
	case "-":
		res = receiver.Num1 - receiver.Num2
	case "*":
		res = receiver.Num1 * receiver.Num2
	case "/":
		res = receiver.Num1 / receiver.Num2
	default:
		fmt.Println("运算符输入有误...")
	}
	return res
}
func main() {
	var test MethodUtils
	test.Print10()
	test.PrintMn(5, 6)
	fmt.Println("area = ", test.Area(20, 8))
	test.test4(5)
	test.test5(3, 2, "x")
	cal := Calculator{10, 5}
	res := cal.test6("/")
	fmt.Println("res = ", res)
}
