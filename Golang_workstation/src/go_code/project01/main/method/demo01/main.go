package main

import "fmt"

type A struct {
	Num int
}

//给A类型绑定方法
func (a A) test() {
	fmt.Println("Test()", a.Num)
}
func main() {
	var a A
	a.Num = 10
	a.test() //调用方法
}
