package main

import (
	"fmt"
	"sort"
)

type Student struct {
	Name    string
	Sex     string
	Address string
}

func main() {
	map1 := map[int]int{
		10: 100,
		1:  13,
		4:  36,
		8:  90,
	}
	fmt.Println(map1)
	//如果按照map的key的顺序进行排序输出
	//1、先将map的key放入到切片中
	//2、对切片排序
	//3、遍历切片，然后按照key来输出map的值

	var keys []int
	for i, _ := range map1 {
		keys = append(keys, i)
	}
	//排序
	sort.Ints(keys)
	fmt.Println(keys)
	for _, v := range keys {
		fmt.Printf("map[%v] = %v ", v, map1[v])
	}
	fmt.Println()
	//key为学号，value为结构体，包含姓名和性别
	stu := map[int]Student{
		1: {
			"张三",
			"男",
			"Chengdu",
		},
		2: {
			"李四",
			"女",
			"Wuhan",
		},
		3: {
			"Tom",
			"男",
			"England",
		},
	}
	fmt.Println(stu[1].Name)
	fmt.Println(stu[2].Name)
	//遍历各个学生的信息
	for i, student := range stu {
		fmt.Printf("学号：%v，姓名：%v，性别：%v，地址：%v\n", i, student.Name, student.Sex, student.Address)
	}
}
