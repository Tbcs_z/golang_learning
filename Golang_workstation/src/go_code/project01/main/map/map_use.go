package main

import "fmt"

func printMap(cityMap map[string]string) {
	//cityMap 是一个引用传递
	for key, value := range cityMap {
		fmt.Println("key = ", key)
		fmt.Println("value = ", value)
	}
}

func ChangValue(cityMap map[string]string) {
	cityMap["England"] = "London"
}

func main() {
	cityMap := make(map[string]string)
	//添加
	cityMap["China"] = "BeiJing"
	cityMap["Japan"] = "Tokyo"
	cityMap["USA"] = "NewYork"
	//删除
	delete(cityMap, "China")
	//修改
	cityMap["USA"] = "DC"
	ChangValue(cityMap)
	//查找
	val, ok := cityMap["Japan"]
	if ok {
		fmt.Printf("有Japan，值为：%v\n", val)
	} else {
		fmt.Println("没有Japan")
	}
	//遍历
	printMap(cityMap)
	//map的长度
	fmt.Println("cityMap 有", len(cityMap), "对key-value")
}
