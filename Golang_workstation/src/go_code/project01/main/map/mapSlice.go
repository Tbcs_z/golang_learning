package main

import "fmt"

func main() {
	/*
		要求：使用一个map来记录monster的信息name和age,也就是说一个monster对应一个map,并
		且妖怪的个数可以动态的增加 => map切片
	*/

	monster := make([]map[string]string, 2)
	//增加第一个信息
	if monster[0] == nil {
		monster[0] = make(map[string]string, 2)
		monster[0]["name"] = "牛魔王"
		monster[0]["age"] = "500岁"
	}
	if monster[1] == nil {
		monster[1] = make(map[string]string, 2)
		monster[1]["name"] = "玉兔精"
		monster[1]["age"] = "400岁"
	}
	//如下写法越界
	//if monster[2] == nil {
	//	monster[2] = make(map[string]string, 2)
	//	monster[2]["name"] = "狐狸精"
	//	monster[2]["age"] = "300岁"
	//}
	//需要使用到切片的append函数，可以动态增加
	newMonster := map[string]string{
		"name": "火云邪神",
		"age":  "1000岁",
	}
	monster = append(monster, newMonster)
	fmt.Println(monster)
}
