package main

import "fmt"

/*
1、使用map[string]map[string]sting的map类型
2、key:表示用户名，是唯一的，不可以重复
3、如果某个用户名存在，就将其密码修改"888888"，如果不存在就增加这个用户
信息，（包括昵称nickname和密码pwd)
4、编写一个函数modifyUser(users map[string]map[string]string,name string),完
成上述功能
*/
func modifyUser(users map[string]map[string]string, name string) {
	if users[name] != nil {
		users[name]["password"] = "88888888"
	} else {
		users[name] = make(map[string]string)
		users[name]["nickName"] = name + "nick"
		users[name]["password"] = "123456"
	}
}
func main() {
	user := map[string]map[string]string{
		"tom": {
			"nickName": "Tompson",
			"password": "123456",
		},
		"Jack": {
			"nickName": "JackSmith",
			"password": "12345678",
		},
	}
	modifyUser(user, "WelSon")
	modifyUser(user, "tom")
	fmt.Println(user["tom"])
	fmt.Println(user["Jack"])
	fmt.Println(user["WelSon"])
}
