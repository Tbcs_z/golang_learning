package main

import "fmt"

func main() {
	/*
		map的三种创建方式
	*/
	//--------------
	//第一种
	//声明myMap是一种map类型  map[key]value
	var myMap map[string]string
	if myMap == nil {
		fmt.Println("空map")
	}
	//在使用map前，需要先用make给map分配数据空间
	myMap = make(map[string]string, 10)
	myMap["one"] = "java"
	myMap["two"] = "C++"
	myMap["three"] = "Python"
	fmt.Println(myMap)

	//第二种声明方式
	myMap2 := make(map[int]string)
	myMap2[1] = "java"
	myMap2[2] = "C++"
	myMap2[3] = "Python"
	fmt.Println(myMap2)
	//第三种声明方式

	myMap3 := map[string]string{
		"one":   "java",
		"two":   "c++",
		"three": "python",
	}
	fmt.Println(myMap3)

	//声明map后，如果没有初始化，需要make为其开辟空间

	//课堂练习：演示一个key-vaue的value是map的案例
	//比如：我们要存放3个学生信息，每个学生有name和sex信息
	//思路：map[string]map[string]string
	Student := make(map[int]map[string]string)
	Student[1] = make(map[string]string, 2)
	Student[1]["name"] = "张三"
	Student[1]["sex"] = "男"
	Student[1]["province"] = "四川"
	Student[2] = make(map[string]string, 2) //这句话不能少
	Student[2]["name"] = "李四"
	Student[2]["sex"] = "女"
	Student[2]["province"] = "重庆"
	fmt.Println(Student[1])
	fmt.Println(Student[2])
	//遍历
	for i, m := range Student {
		fmt.Printf("k1 = %v\n", i)
		for s, s2 := range m {
			fmt.Printf("\t k2 = %v,v2 = %v\n", s, s2)
		}
	}
}
