package implService

import (
	"fmt"
	"go_code/project01/main/cunstomer_MS/main/model"
)

//完成对Customer的操作，包括增删改查
type CustomerService struct {
	customers []model.Customer
	//表示当前有多少个用户,后面还可以作为新客户的编号
	customerNum int
}

func NewCustomerService() *CustomerService {
	customerService := &CustomerService{}
	customerService.customerNum = 1
	customer := model.NewCustomer(1, "张三", "男", 23, "010-800820", "dsad@gmail.com")
	customerService.customers = append(customerService.customers, customer)
	return customerService
}

//返回客户切片
func (s CustomerService) List() []model.Customer {
	return s.customers
}

//添加客户
func (s *CustomerService) Add(customer model.Customer) bool {
	s.customerNum++
	customer.Id = s.customerNum
	s.customers = append(s.customers, customer)
	return true
}

//修改客户
func (s *CustomerService) Update(id int) bool {
	index := s.FindById(id)
	if index == -1 {
		fmt.Println("未查询到此ID")
		return false
	}
	fmt.Println("<直接回车表示不修改>")
	fmt.Printf("姓名(%v):", s.customers[index].Name)
	fmt.Scanln(&s.customers[index].Name)
	fmt.Printf("性别(%v):", s.customers[index].Sex)
	fmt.Scanln(&s.customers[index].Sex)
	fmt.Printf("年龄(%v):", s.customers[index].Age)
	fmt.Scanln(&s.customers[index].Age)
	fmt.Printf("电话(%v):", s.customers[index].Tel)
	fmt.Scanln(&s.customers[index].Tel)
	fmt.Printf("邮箱(%v):", s.customers[index].Email)
	fmt.Scanln(&s.customers[index].Email)
	return true
}

//删除客户
func (s *CustomerService) Delete(id int) bool {
	index := s.FindById(id)
	if index == -1 {
		return false
	}
	//如何从切片中删除一个元素
	s.customers = append(s.customers[:index], s.customers[index+1:]...)
	//j := 0
	//for _, v := range s.customers {
	//	if v != s.customers[index] {
	//		s.customers[j] = v
	//		j++
	//	}
	//}
	//s.customers = s.customers[:j]
	return true
}

//根据ID查找是否存在该用户
func (s CustomerService) FindById(id int) int {
	index := -1
	for i, i2 := range s.customers {
		if i2.Id == id {
			index = i
		}
	}
	return index
}
func (s CustomerService) FindCus(id int) string {
	index := s.FindById(id)
	if index == -1 {
		return ""
	}
	return s.customers[index].Show()
}
