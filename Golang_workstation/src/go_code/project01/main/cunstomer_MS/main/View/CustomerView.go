package main

import (
	"fmt"
	"go_code/project01/main/cunstomer_MS/main/implService"
	"go_code/project01/main/cunstomer_MS/main/model"
)

type CustomerView struct {
	Exit   bool
	Choice string
	//增加一个customerService字段
	customerService *implService.CustomerService
}

//显示所有客户信息
func (v CustomerView) List() {
	//获取当前所有的客户信息
	customer := v.customerService.List()
	fmt.Println("--------------------------客户信息--------------------------")
	fmt.Println("编号\t姓名\t性别\t年龄\t电话\t\t邮箱")
	for _, i2 := range customer {
		fmt.Println(i2.Show())
	}
}
func (v *CustomerView) Add() {
	fmt.Println("--------------------------添加客户--------------------------")
	fmt.Print("姓名：")
	name := ""
	fmt.Scanln(&name)
	fmt.Print("性别：")
	sex := ""
	fmt.Scanln(&sex)
	fmt.Print("年龄：")
	Age := 0
	fmt.Scanln(&Age)
	fmt.Print("电话：")
	tel := ""
	fmt.Scanln(&tel)
	fmt.Print("邮箱：")
	email := ""
	fmt.Scanln(&email)
	//构建一个新的customer
	//ID是唯一的需要系统分配
	customer := model.NewCustomer2(name, sex, Age, tel, email)
	if v.customerService.Add(customer) {
		fmt.Println("添加完成")
	} else {
		fmt.Println("添加失败！")
	}
}
func (v CustomerView) Update() {
	fmt.Println("------------------------修改客户信息------------------------")
	fmt.Print("请输入需要修改的客户ID(-1表示退出)：")
	id := 0
	fmt.Scanln(&id)
	if id == -1 || !v.customerService.Update(id) {
		fmt.Println("修改失败")
		return
	}
	fmt.Println("修改成功！")
}
func (v CustomerView) Delete() {
	fmt.Println("------------------------删除客户信息------------------------")
	fmt.Print("请输入需要删除的客户ID：")
	id := 0
	fmt.Scanln(&id)
	if id == -1 {
		return
	}
	flag := ""
	fmt.Print("是否确认删除？(y/n)：")
	fmt.Scanln(&flag)
	if flag == "y" || flag == "Y" {
		if v.customerService.Delete(id) {
			fmt.Println("删除成功！")
		} else {
			fmt.Println("未查询到此ID，删除失败！")
		}
	} else if flag == "n" || flag == "N" {
		return
	} else {
		fmt.Println("输入错误，已返回主菜单")
	}

}
func (v CustomerView) Find() {
	fmt.Println("------------------------查找客户信息------------------------")
	fmt.Print("请输入需要查找的客户ID：")
	id := 0
	fmt.Scanln(&id)
	if v.customerService.FindCus(id) == "" {
		fmt.Println("未查询到此ID！")
	} else {
		fmt.Println("--------------------------客户信息--------------------------")
		fmt.Println("编号\t姓名\t性别\t年龄\t电话\t\t邮箱")
		fmt.Println(v.customerService.FindCus(id))
	}

}
func (v CustomerView) mainMenu() {
	for v.Exit {
		fmt.Println("----------------------客户信息管理系统----------------------")
		fmt.Println("\t1、查看客户信息")
		fmt.Println("\t2、增加客户信息")
		fmt.Println("\t3、修改客户信息")
		fmt.Println("\t4、删除客户信息")
		fmt.Println("\t5、查找客户信息")
		fmt.Println("\t0、退       出")
		fmt.Print("\t请选择(0-5)：")
		fmt.Scanln(&v.Choice)
		switch v.Choice {
		case "1":
			v.List()
		case "2":
			v.Add()
		case "3":
			v.Update()
		case "4":
			v.Delete()
		case "5":
			v.Find()
		case "0":
			for v.Exit {
				flag := ""
				fmt.Print("是否确认退出？(y/n)：")
				fmt.Scanln(&flag)
				if flag == "y" || flag == "Y" {
					v.Exit = false
				} else if flag == "n" || flag == "N" {
					break
				} else {
					fmt.Println("输入错误，请重新输入！")
				}
			}
			break
		default:
			fmt.Println("输入错误，请重新输入！")
		}
	}
	fmt.Println("exited......")
}

func main() {
	customer := CustomerView{
		Exit:   true,
		Choice: "",
	}
	//完成初始化
	customer.customerService = implService.NewCustomerService()
	//显示主菜单
	customer.mainMenu()
}
