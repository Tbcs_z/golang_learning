package model

import "fmt"

//声明结构体表示客户信息
type Customer struct {
	Id    int
	Name  string
	Sex   string
	Age   int
	Tel   string
	Email string
}

//工厂模式，返回一个customer的示例
func NewCustomer(Id int, Name string,
	Sex string, Age int, Tel string, Email string) Customer {
	return Customer{
		Id:    Id,
		Name:  Name,
		Sex:   Sex,
		Age:   Age,
		Tel:   Tel,
		Email: Email,
	}
}

//第二个不带ID 的newcustomer方法
func NewCustomer2(Name string,
	Sex string, Age int, Tel string, Email string) Customer {
	return Customer{
		Name:  Name,
		Sex:   Sex,
		Age:   Age,
		Tel:   Tel,
		Email: Email,
	}
}
func (c Customer) Show() string {
	info := fmt.Sprintf("%v\t%v\t%v\t%v\t%v\t%v", c.Id, c.Name, c.Sex, c.Age, c.Tel, c.Email)
	return info
}
func (c *Customer) Update(Name string,
	Sex string, Age int, Tel string, Email string) {
	c.Name = Name
	c.Sex = Sex
	c.Age = Age
	c.Tel = Tel
	c.Email = Email
}
