package main

import "fmt"

func printArray1(myArray []int) {
	//引用传递
	// _ 表示匿名的变量
	for _, value := range myArray {
		fmt.Println("value =", value)
	}

	myArray[0] = 100
}

//动态数组本身就是一个指向自己的指针或引用
func main() {
	myArray1 := []int{1, 2, 3, 4} //动态数组，切片 slice

	fmt.Printf("myArray1 is %T\n", myArray1)

	printArray1(myArray1)
	fmt.Println("----------")
	for _, value := range myArray1 {
		fmt.Println("value =", value)
	}
}
