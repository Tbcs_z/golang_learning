package main

import (
	"fmt"
)

func main() {
	str := "helloGolang"
	//使用切片获取Golang
	slice := str[5:]
	fmt.Println("slice =", slice)
	//string本身不可变,如下方式会报错
	//str[o] = 'z'
	//可通过以下方式修改
	slice2 := []byte(str)
	//先转成一个切片，修改后再转成字符串
	slice2[0] = 'z'
	fmt.Println(slice2)
	str = string(slice2)
	fmt.Println(str)
	//细节：如果转成byte切片后，可以处理英文和数字，但是无法处理中文
	//原因：byte按一个字节处理，而一个汉字占3个字节，因此会出现乱码
	//解决方法：将string转成 []rune 即可,rune是按字符处理，兼容汉字
	slice3 := []rune(str)
	slice3[0] = '北'
	fmt.Println(slice3)
	str = string(slice3)
	fmt.Println(str)

	arrr := fbn(-1)
	fmt.Println(arrr)
}
func fbn(n int) []uint64 {
	if n < 0 {
		fmt.Println("输入错误")
		return nil
	}
	arr := make([]uint64, n)
	if n == 0 {
		return arr
	} else if n == 1 {
		arr[0] = 1
		return arr
	}
	arr[0] = 1
	arr[1] = 1
	for i := 2; i < n; i++ {
		arr[i] = arr[i-1] + arr[i-2]
	}
	return arr
}
