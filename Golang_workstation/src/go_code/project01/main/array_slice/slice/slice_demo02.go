package main

import "fmt"

func main() {
	var numbers = make([]int, 3, 5) //后两个参数为长度和容量
	fmt.Printf("len = %d,Cap = %d ,Slice = %v\n", len(numbers), cap(numbers), numbers)
	//向number切片追加一个元素，number len =4， [0,0,0,1],cap =5
	numbers = append(numbers, 1)
	fmt.Printf("len = %d,Cap = %d ,Slice = %v\n", len(numbers), cap(numbers), numbers)
	numbers = append(numbers, 2)
	fmt.Printf("len = %d,Cap = %d ,Slice = %v\n", len(numbers), cap(numbers), numbers)
	//向一个容量已经满的切片数组追加  cap会一次性增加5，长度会变成6
	numbers = append(numbers, 3)
	fmt.Printf("len = %d,Cap = %d ,Slice = %v\n", len(numbers), cap(numbers), numbers)
	fmt.Println("________________")
	var number2 = make([]int, 3) //不指定长度
	fmt.Printf("len = %d,Cap = %d ,Slice = %v\n", len(number2), cap(number2), number2)
	number2 = append(number2, 1) //此时容量为6
	fmt.Printf("len = %d,Cap = %d ,Slice = %v\n", len(number2), cap(number2), number2)
	//切片的长度和容量不同，长度表示左指针至右指针之间的距离，容量表示左指针至底层数组末尾的距离
	//切片的扩容机制，append的时候，如果长度超增加后超过容量，则将容量增加两倍
}
