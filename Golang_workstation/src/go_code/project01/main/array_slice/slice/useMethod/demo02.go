package main

import "fmt"

func main() {
	//切片的遍历
	//1、使用常规的for循环遍历
	slice := make([]int, 5, 10)
	slice2 := []int{10, 20, 30, 40, 50}
	for i := 0; i < len(slice); i++ {
		fmt.Printf("slice[%v] = %v\n", i, slice[i])
	}
	//2、使用 for range 遍历
	for i, i2 := range slice {
		fmt.Printf("index = %d,value = %d\n", i, i2)
	}
	//切片可以继续切片
	slice3 := slice2[1:3]
	fmt.Println(slice3)
}
