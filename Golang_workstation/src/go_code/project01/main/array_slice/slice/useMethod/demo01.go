package main

import "fmt"

func main() {
	//方式二
	//对于切片，make必须使用
	var slice []float64 = make([]float64, 5, 10)
	slice[2] = 10
	slice[4] = 20
	fmt.Println("cap = ", cap(slice))
	fmt.Println("len = ", len(slice))
	//方式三
	var slice1 []string = []string{"tom", "kacd", "dsajk"}
	fmt.Println(slice1)
	fmt.Println("len = ", len(slice1))
	fmt.Println("cap = ", cap(slice1))
}
