package main

import "fmt"

func main() {
	//声明slice是一个切片，并且初始化默认值是1，2，3 长度为3
	//slice1 := []int{1, 2, 3}
	//var slice1 []int
	var slice = make([]int, 3)
	slice2 := make([]int, 3)
	// %v 表示打印出全部的详细信息
	fmt.Printf("length = %d,slice = %v,cap = %v\n", len(slice), slice, cap(slice))
	slice1 := make([]int, 3) //开辟三个空间，默认值为0
	slice1[0] = 199
	fmt.Printf("length = %d,slice = %v\n", len(slice1), slice1)
	fmt.Printf("length = %d,slice = %v\n", len(slice2), slice2)

	//判断一个slice是否为空
	if slice1 == nil {
		fmt.Println("slice1 是一个空切片")
	} else {
		fmt.Println("slice1 不是一个空切片")

	}
}
