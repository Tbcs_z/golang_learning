package main

import "fmt"

func main() {
	//定义/声明二维数组
	var arr [4][6]int
	arr[1][2] = 1
	arr[2][1] = 2
	arr[2][3] = 3
	fmt.Println(arr)
	//遍历二维数组
	for i := 0; i < 4; i++ {
		for j := 0; j < 6; j++ {
			fmt.Print(arr[i][j], " ")
		}
		fmt.Println()
	}
	var arr1 [2][3]int = [2][3]int{{1, 2, 3}, {4, 5, 6}}
	fmt.Println(arr1)
	arr2 := [2][3]int{{4, 5, 6}, {7, 8, 9}}
	fmt.Println(arr2)
	arr3 := [...][2]int{{1, 2}, {3, 4}}
	fmt.Println(arr3)
}
