package main

import "fmt"

func main() {

	arr := [2][3]int{{4, 5, 6}, {7, 8, 9}}
	//1、for 循环
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			fmt.Print(arr[i][j], " ")
		}
		fmt.Println()
	}
	//2、for range 方式
	for i, ints := range arr {
		//ints 是一个一维数组
		for i2, i3 := range ints {
			fmt.Printf("arr[%v][%v] = %v ", i, i2, i3)
		}
		fmt.Println()
	}
	//定义二维数组，用于保存三个班，每个班五名同学成绩
	//并求出每个班级平均分，以及所有班级平均分
	class := [3][5]float64{}
	total1 := 0.00
	total2 := 0.00
	total3 := 0.00
	for i := 0; i < len(class); i++ {
		for j := 0; j < len(class[i]); j++ {
			fmt.Printf("请输入：第%v班的第%v个学生的成绩：", i+1, j+1)
			fmt.Scanln(&class[i][j])
			switch i {
			case 0:
				total1 += class[i][j]
			case 1:
				total2 += class[i][j]
			case 2:
				total3 += class[i][j]
			}
		}
		switch i {
		case 0:
			fmt.Printf("第%d 班级的总分为：%v，平均分为：%v\n", i+1, total1, total1/5)
		case 1:
			fmt.Printf("第%d 班级的总分为：%v，平均分为：%v\n", i+1, total2, total2/5)
		case 2:
			fmt.Printf("第%d 班级的总分为：%v，平均分为：%v\n", i+1, total3, total3/5)
		}
	}
	fmt.Printf("所有班级总平均分为：%.2f\n", (total1+total2+total3)/5/3)
	fmt.Println(class)
}
