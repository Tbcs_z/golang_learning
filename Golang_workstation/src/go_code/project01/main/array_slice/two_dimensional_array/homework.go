package main

import (
	"fmt"
	"math/rand"
	"time"
)

func ques01() {
	/*
		随机生成10个整数（1~100的范围）保存到数组
		并倒序打印以及求平均值、求最大值和最大值的下标，并查找里面是否有55
	*/
	rand.Seed(time.Now().UnixNano())
	var arr [10]int
	for i := 0; i < 10; i++ {
		arr[i] = rand.Intn(100)
	}
	fmt.Println(arr)
	var value int
	var index int
	var avg int
	var flag bool
	for i := 9; i >= 0; i-- {
		fmt.Printf("%v ", arr[i])
		if arr[i] > value {
			value = arr[i]
			index = i
		}
		if arr[i] == 55 {
			flag = true
		}
		avg += arr[i]
	}
	if flag {
		fmt.Printf("\n平均值为：%v,最大值为: %v，最大值下标为：%v，数组中有55", avg/10, value, index)
	} else {
		fmt.Printf("\n平均值为：%v,最大值为: %v，最大值下标为：%v，数组中没有55", avg/10, value, index)
	}
}
func ques02() {
	/*
		已知有个排序好（升序）的数组，要求插入一个元素，
		最后打印该数组，顺序依然是升序
	*/
	arr := []int{1, 2, 3, 4, 10}
	arr = append(arr, 5)
	for i := 0; i < len(arr)-1; i++ {
		for j := 0; j < len(arr)-i-1; j++ {
			if arr[j] > arr[j+1] {
				temp := arr[j+1]
				arr[j+1] = arr[j]
				arr[j] = temp
			}
		}
	}
	fmt.Println(arr)
}
func ques03() {
	/*
		定义一个3行4列的二维数组，逐个从键盘输入值，编写程序将四周的数据清0
	*/
	var arr [3][4]int
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			fmt.Printf("请输入：第%v行的第%v个数：", i+1, j+1)
			fmt.Scanln(&arr[i][j])
		}
	}
	fmt.Println(arr)
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			if i == 1 && (j == 1 || j == 2) {
				fmt.Printf("%v ", arr[i][j])
				continue
			}
			arr[i][j] = 0
			fmt.Printf("%v ", arr[i][j])
		}
		fmt.Println()
	}
	fmt.Println(arr)
}
func ques04() {
	/*
		定义一个4行4列的二维数组，逐个从键盘输入值，
		然后将第1行和第4行的数据进行交换，将第2行和第3行的数据进行交换
	*/
	var arr [4][4]int
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			fmt.Printf("请输入：第%v行的第%v个数：", i+1, j+1)
			fmt.Scanln(&arr[i][j])
		}
	}
	fmt.Println("交换前：", arr)
	temp := arr[0]
	arr[0] = arr[3]
	arr[3] = temp
	temp = arr[1]
	arr[1] = arr[2]
	arr[2] = temp
	fmt.Println("交换后：", arr)
}
func ques05() {
	/*
		试保存13579五个奇数到数组，并倒序打印
	*/
	arr := []int{1, 3, 5, 7, 9}
	for i := 4; i >= 0; i-- {
		fmt.Print(arr[i], " ")
	}
}
func ques06() {
	/*
		试写出实现查找的核心代码，比如已知数组arr[10]string里面保存了十个元素，
		现要查找"AA"在其中是否存在，打印提示，如果有多个”AA”,也要找到对应的下标。
	*/
	arr := [10]string{"AA", "BB", "CC", "aa", "xx", "AA", "xx", "BB", "CC", "aa"}
	var index []int
	var flag bool
	for i, s := range arr {
		if s == "AA" {
			index = append(index, i)
			flag = true
		}
	}
	if flag {
		fmt.Println("数组中有”AA“，下标为：", index)
	} else {
		fmt.Println("数组中没有”AA“")
	}
}
func ques07() {
	/*
		随机生成10个整数(1-100之间)，使用冒泡排序法进行排序，
		然后使用二分查找法，查找是否有90这个数，并显示下标，
		如果没有则提示“找不到该数”
	*/
	rand.Seed(time.Now().UnixNano())
	var arr [10]int
	for i := 0; i < 10; i++ {
		arr[i] = rand.Intn(100)
	}
	//冒泡
	for i := 0; i < len(arr)-1; i++ {
		for j := 0; j < len(arr)-1-i; j++ {
			if arr[j] > arr[j+1] {
				temp := arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = temp
			}
		}
	}
	halfSearch(arr, 0, len(arr)-1, 90)
	fmt.Println(arr)
}
func halfSearch(arr [10]int, startIndex, endIndex, num int) {
	if startIndex > endIndex {
		fmt.Println("该数组没有90~")
		return
	}
	middle := (startIndex + endIndex) / 2
	if num < arr[middle] {
		endIndex = middle - 1
		halfSearch(arr, startIndex, endIndex, num)
	} else if num > arr[middle] {
		startIndex = middle + 1
		halfSearch(arr, startIndex, endIndex, num)
	} else {
		fmt.Println("该数组有90，下标为：", middle)
	}

}
func ques08(arr [5]int) {
	/*
		编写一个函数，可以接收一个数组，
		该数组有5个数，请找出最大的数和最小的数和对应的数组下标是多少？
	*/
	var max, min, maxIndex, minIndex int
	min = 1
	for i := 0; i < len(arr); i++ {
		if arr[i] > max {
			max = arr[i]
			maxIndex = i
		}
		if arr[i] < min {
			min = arr[i]
			minIndex = i
		}
	}
	fmt.Printf("该数组最大值为：%v,下标为：%v,最小值为：%v,下标为:%v", max, maxIndex, min, minIndex)
}
func ques09() {
	arr := [8]int{1, 2, 3, 4, 5, 6, 7, 8}
	total := 0
	for _, i2 := range arr {
		total += i2
	}
	avg := float64(total) / 8
	bigger := 0
	smaller := 0
	fmt.Println(avg)
	for _, i2 := range arr {
		if float64(i2) > avg {
			bigger++
		} else if float64(i2) < avg {
			smaller++
		}
	}
	fmt.Printf("小于平均值的个数有：%v个，大于平均值的个数有：%v个", smaller, bigger)
}
func score(arr [8]float64) (max, min float64, maxIndex, minIndex int) {
	max, min = arr[0], arr[0]
	for i := 0; i < len(arr); i++ {
		if arr[i] > max {
			max = arr[i]
			maxIndex = i
		}
		if arr[i] < min {
			min = arr[i]
			minIndex = i
		}
	}
	return max, min, maxIndex, minIndex
}
func ques10() {
	var arr [8]float64
	for i := 0; i < len(arr); i++ {
		fmt.Printf("请 %v 号评委打分：", i+1)
		fmt.Scanln(&arr[i])
	}
	max, min, maxIndex, minIndex := score(arr)
	var total float64
	for i := 0; i < len(arr); i++ {
		total += arr[i]
	}
	fmt.Printf("最高分为：%v 号评委的 %v 分\n", maxIndex+1, max)
	fmt.Printf("最低分为：%v 号评委的 %v 分\n", minIndex+1, min)
	avg := (total - max - min) / 6
	var gap [8]float64
	for i, f := range arr {
		if f > avg {
			gap[i] = f - avg
		}
		if f < avg {
			gap[i] = avg - f
		}
	}
	fmt.Println(gap)
	_, _, worst, best := score(gap)

	fmt.Printf("最终得分：%.2f 分\n最佳评委是：%v 号评委，最差评委是：%v 号评委", avg, best+1, worst+1)
}
func main() {
	ques10()
}
