package main

import "fmt"

func halfSearch(arr []int, startIndex, endIndex, num int) {
	if startIndex > endIndex {
		fmt.Println("不好意思，没找到~")
		return
	}
	middle := (startIndex + endIndex) / 2
	if num < arr[middle] {
		endIndex = middle - 1
		halfSearch(arr, startIndex, endIndex, num)
	} else if num > arr[middle] {
		startIndex = middle + 1
		halfSearch(arr, startIndex, endIndex, num)
	} else {
		fmt.Println("找到了，下标为：", middle)
	}

}
func main() {
	/*
		2、请对一个有序数组进行二分查找{1,8,10,89,100,1314 }
			输入一个数看看该数组是否存在此数，并求出下表，如果没有就提示“没有这个数”（会使用到递归）
	*/
	arr1 := []int{1, 8, 10, 89, 100, 1314}
	fmt.Print("输入要查找的数：")
	num := 0
	fmt.Scanln(&num)
	halfSearch(arr1, 0, len(arr1)-1, num)
}
