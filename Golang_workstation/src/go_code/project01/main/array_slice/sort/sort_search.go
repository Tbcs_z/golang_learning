package main

import (
	"fmt"
)

func main() {
	//案例演示：
	/*
		1、有一个数列：白眉鹰王、金毛狮王、紫衫龙王、青翼蝠王
		猜数游戏：从键盘中任意输入一个名称，判断数列中是否包含此名称（顺序查找）
	*/
	arr := [4]string{"白眉鹰王", "金毛狮王", "紫衫龙王", "青翼蝠王"}
	fmt.Print("输入名称：")
	q := ""
	fmt.Scanln(&q)
	for i := 0; i < len(arr); i++ {
		if q == arr[i] {
			fmt.Println("已找到", arr[i], "下标为：", i)
			break
		} else if i == len(arr)-1 {
			fmt.Println("没有找到~")
		}
	}
	//第二种方式
	index := -1
	for i := 0; i < len(arr); i++ {
		if q == arr[i] {
			index = i //将找到的值对应的下标赋给index
		}
	}
	if index != -1 {
		fmt.Printf("找到%v,下标为：%v", q, index)
	}

}
