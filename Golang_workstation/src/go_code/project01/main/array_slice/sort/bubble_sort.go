package main

import "fmt"

func bubble(arr *[10]int) {
	for i := 0; i < len(arr)-1; i++ {
		for j := 0; j < len(arr)-1-i; j++ {
			if arr[j] > arr[j+1] {
				temp := arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = temp
			}
		}
	}
	fmt.Println(*arr)
}
func main() {
	arr := [10]int{1, 3, 2, 42, 6, 123, 21, 421, 4, 124}
	fmt.Println(arr)
	bubble(&arr)
}
