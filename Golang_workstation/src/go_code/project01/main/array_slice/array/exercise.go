package main

import "fmt"

func main() {
	//从终端循环输入5个数字，并保存到float64数组，然后输出
	//var arr [5]float64
	//for i := 0; i < 5; i++ {
	//	fmt.Printf("输入数字%d：", i+1)
	//	fmt.Scan(&arr[i])
	//}
	//fmt.Println(arr)
	var arr1 [3]int = [3]int{1, 2, 3}
	var arr2 = [3]int{1, 2, 3}
	var arr3 = [...]int{1, 2, 3} // 这里的 ... 是固定写法
	//可以指定元素对应的下标
	var arr4 = [3]string{1: "tom", 2: "jack", 0: "jenny"}
	fmt.Println(arr1)
	fmt.Println(arr2)
	fmt.Println(arr3)
	fmt.Println(arr4)
	for i, v := range arr4 {
		fmt.Printf("i = %v,v = %v\n", i, v)
	}
}
