package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//1、创建一个byte类型的26个元素的数组，分别放置A~Z，使用for循环访问所有元素并打印出来，提示：字符数据运算'A' + 1 -> 'B'
	var word [26]byte
	for i, _ := range word {
		word[i] = 'A' + byte(i)
	}
	for i, _ := range word {
		fmt.Printf("%c ", word[i])
	}
	//2、求出一个数组的最大值，并得到对应的下标
	var index int
	var Max byte
	for i, b := range word {
		if Max < b {
			Max = b
			index = i
		}
	}
	fmt.Printf("\n最大值为：%d，下标为：%d", Max, index)
	//3、求出一个数组的和 以及平均值，使用for range
	sum := 0
	for _, v := range word {
		sum += int(v)
	}
	avg := sum / len(word)
	fmt.Printf("\n数组和为：%d，平均值为：%.2f\n", sum, float64(avg))
	//4、随机生成5个数，并将其反转打印
	var arr [5]int
	len := len(arr)
	//为了每次生成的随机数不一样，需要给一个Seed值，且Seed值每次也不一样
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < len; i++ {
		arr[i] = rand.Intn(100) //生成100以内的随机数
	}
	fmt.Println(arr)
	//交换的次数，是 len/2 ,倒数第一个和第一个元素交换
	temp := 0
	for i := 0; i < len/2; i++ {
		temp = arr[len-1-i]
		arr[len-1-i] = arr[i]
		arr[i] = temp
	}
	fmt.Println(arr)
}
