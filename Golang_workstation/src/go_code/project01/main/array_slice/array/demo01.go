package main

import "fmt"

func main() {
	/*
		一个养鸡场有6只鸡，它们的体重分别是3kg,5kg,1kg,
		3.4kg,2kg,50kg。请问这六只鸡的总体重是多少？平
		均体重是多秒？请你编一个程序。=》数组
	*/
	//一、用变量表示
	hen1 := 3.0
	hen2 := 5.0
	hen3 := 1.0
	hen4 := 3.4
	hen5 := 2.0
	hen6 := 50.0
	totaWeight := hen1 + hen2 + hen3 + hen4 + hen5 + hen6
	avgWeight := fmt.Sprintf("%.2f", totaWeight/6)
	fmt.Printf("总体重：%vkg,平均体重：%vkg\n", totaWeight, avgWeight)
	//二、数组
	//定义一个数组
	arr := [6]float64{3.0, 5.0, 1.0, 3.4, 2.0, 50.0}
	//2、给数组的每个元素赋值
	//arr[0] = 22.2
	//3、遍历数组
	var totalweight2 float64
	//1\
	for i, _ := range arr {
		fmt.Print(arr[i], " ")
		totalweight2 += arr[i]
	}
	//2\
	//for i := 0; i < len(arr); i++ {
	//	totalweight2 += arr[i]
	//}
	fmt.Println()
	fmt.Printf("总体重：%vkg,平均体重：%vkg\n", totalweight2, avgWeight)
}
