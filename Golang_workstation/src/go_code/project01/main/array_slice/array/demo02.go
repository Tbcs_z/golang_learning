package main

import "fmt"

func main() {
	var intArr [3]int
	//当定义完数组后，数组的各个元素已经有默认值  0
	fmt.Println(intArr) //[0 0 0]
	intArr[0] = 10
	intArr[1] = 20
	intArr[2] = 30
	//数组名的地址为数组首元素的地址，第二个元素的地址即第一个元素地址加上占用的字节数
	fmt.Printf("address = %p , intArr[0] = %p，intArr[1] = %p", &intArr, &intArr[0], &intArr[1])
	fmt.Println("\n", intArr)
}
