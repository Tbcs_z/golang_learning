package main

import "fmt"

type Human struct {
	name string
	sex  string
}

func (this Human) Eat() {
	fmt.Println("Human.Eat()...")
}
func (this Human) Walk() {
	fmt.Println("Human.Walk()...")
}

//================
type SuperMan struct {
	Human //SuperMan类继承了human类的方法

	level int
}

//重定义父类的方法Eat()
func (this *SuperMan) Eat() {
	fmt.Println("SuperHuman.Eat()...")
}

//子类的新方法
func (this *SuperMan) Fly() {
	fmt.Println("SuperMan.fly()....")
}
func (this *SuperMan) Print() {
	fmt.Println("name = ", this.name)
	fmt.Println("sex = ", this.sex)
	fmt.Println("level = ", this.level)
}
func main() {
	h := Human{"zhangsan", "female"}
	h.Eat()
	h.Walk()
	//定义一个子类对象
	//s := SuperMan{Human{"li4", "female"}, 98}
	var s SuperMan
	s.name = "li4"
	s.sex = "female"
	s.level = 99
	s.Walk() //父类的方法
	s.Eat()  //子类的方法
	s.Fly()  //子类的方法
	s.Print()
}
