package main

import "fmt"

//如果类名首字母大写，表示其他包也能访问
type Hero struct {
	//如果说类的属性首字母大写，表示该属性是对外界能够访问的，否则的话智能够类的内部访问
	Name  string
	Ad    int
	Level int
}

//func (this Hero) show() {
//	fmt.Println("Name = ", this.Name)
//	fmt.Println("Ad = ", this.Ad)
//	fmt.Println("Level = ", this.Level)
//
//}
//func (this Hero) GetName() {
//	fmt.Println("Name = ", this.Name)
//}
//func (this Hero) SetName(newName string) {
//	//this 是调用该方法的对象的一个副本（拷贝）
//	this.Name = newName
//}
func (this *Hero) Show() {
	fmt.Println("Name = ", this.Name)
	fmt.Println("Ad = ", this.Ad)
	fmt.Println("Level = ", this.Level)

}
func (this *Hero) GetName() string {
	fmt.Println("Name = ", this.Name)
	return this.Name
}
func (this *Hero) SetName(newName string) {
	//this 是调用该方法的对象的一个副本（拷贝）
	this.Name = newName
}
func main() {
	//面向对象类的表示与封装
	hero := Hero{Name: "zhangsan", Ad: 100, Level: 100}
	hero.Show()
	hero.SetName("li4")
	hero.Show()
}
