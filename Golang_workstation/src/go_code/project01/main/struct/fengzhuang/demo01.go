package main

import "fmt"

//面向对象三要素：继承、多态、封装
//声明一种行的数据类型 myint 是int的一个别名
type myint int

//定义一个结构体
type Book struct {
	title string
	autn  string
}

func changeBook(book Book) {
	//传递一个book的副本
	book.autn = "666"
}
func changeBook2(book *Book) {
	//指针传递
	book.autn = "666"
}
func main() {
	var a myint = 10
	fmt.Println("a= ", a)
	fmt.Printf("a= %T \n", a)
	var book1 Book
	book1.title = "Golang"
	book1.autn = "zhang3"
	fmt.Printf("%v \n", book1)
	changeBook(book1)
	fmt.Printf("%v \n", book1)
	changeBook2(&book1)
	fmt.Printf("%v \n", book1)

}
