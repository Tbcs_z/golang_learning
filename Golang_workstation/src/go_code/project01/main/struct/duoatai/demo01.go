package main

import "fmt"

//本质是一个指针
type AnimalIF interface {
	Sleep()
	GetColor() string //获取动物颜色
	GetType() string  //获取动物种类
}

//具体的类
type Cat struct {
	color string //猫的颜色
}

func (this *Cat) Sleep() {
	fmt.Println("Cat is Sleep")
}
func (this *Cat) GetColor() string {
	return this.color
}
func (this *Cat) GetType() string {
	return "Cat"
}

//具体的类
type Dog struct {
	color string
}

func (this *Dog) Sleep() {
	fmt.Println("Cat is Sleep")
}
func (this *Dog) GetColor() string {
	return this.color
}
func (this *Dog) GetType() string {
	return "Dog"
}

func ShowAnimal(animal AnimalIF) {
	animal.Sleep() //多态
	fmt.Println("Color = ", animal.GetColor())
	fmt.Println("Kind = ", animal.GetType())

}
func main() {
	//var animal AnimalIF //接口的数据类型，父类指针
	//animal = &Cat{"Green"}
	//animal.Sleep() //调用的就是Cat的Sleep()方法.多态的现象
	//animal = &Dog{"Black"}
	//animal.Sleep() //调用Dog的sleep方法，多态的现象
	cat := Cat{"Green"}
	dog := Dog{"blue"}
	ShowAnimal(&cat)
	ShowAnimal(&dog)
}

/*
	多态的基本要素：
		1、有一个父类（有接口）
		2、有子类（实现了父类的全部接口方法）
		3、父类类型的变量（指针）执行子类的具体数据变量
*/
