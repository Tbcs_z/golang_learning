package main

import "fmt"

//定义一个Cat结构体，将Cat的各个字段/属性信息，放入到Cat结构体中
type Cat struct {
	Name  string
	Age   int
	Color string
	Food  string
}

func main() {
	var cat Cat
	cat.Name = "小花"
	cat.Age = 12
	cat.Color = "橙色"
	cat.Food = "吃<。)#)))≦"
	fmt.Println(cat.Name)
	fmt.Println(cat.Age)
	fmt.Println(cat.Color)
	fmt.Println(cat.Food)
	cat1 := Cat{
		Name:  "小白",
		Age:   4,
		Color: "白色",
	}
	fmt.Println(cat1)
}
