package main

import "fmt"

type Student struct {
	Name   string
	Gender string
	Age    int
	Id     int
	Score  float64
}
type Dog struct {
	Name   string
	Age    int
	Weight float64
}
type Box struct {
	Length float64
	Width  float64
	Height float64
}
type Visitor struct {
	Age int
}

func (stu *Student) Say() string {
	info := fmt.Sprintf("Id:%v\nName:%v\nGender:%v\nAge:%v\nScore:%v\n", stu.Id, stu.Name, stu.Gender, stu.Age, stu.Score)
	return info
}
func (dog *Dog) Say() string {
	info := fmt.Sprintf("Name:%v\nAge:%v\nWeight:%vkg\n", dog.Name, dog.Age, dog.Weight)
	return info
}
func (box Box) Size() float64 {
	return box.Length * box.Width * box.Height
}
func (v Visitor) Ticket() int {
	if v.Age > 18 {
		return 20
	} else {
		return 0
	}
}
func main() {
	//stu := Student{
	//	Name:   "小明",
	//	Gender: "男",
	//	Age:    17,
	//	Id:     1001,
	//	Score:  753.5,
	//}
	//fmt.Println(stu.Say())
	//dog := Dog{
	//	Name:   "旺财",
	//	Age:    5,
	//	Weight: 45.7,
	//}
	//fmt.Println(dog.Say())
	//var box Box
	//fmt.Print("输入长：")
	//fmt.Scanln(&box.Length)
	//fmt.Print("输入宽：")
	//fmt.Scanln(&box.Width)
	//fmt.Print("输入高：")
	//fmt.Scanln(&box.Height)
	//fmt.Println("box的体积为：", box.Size())
	vi := Visitor{20}
	fmt.Printf("门票价格为：%d 元\n", vi.Ticket())
}
