package main

import "fmt"

//如果结构体的字段类型是：指针，slice，和map的零值都是nil，即还没有分配空间
//如果需要使用这样的字段，需要先make才能使用
type Person struct {
	Name   string
	Age    int
	Scores [5]float64
	ptr    *int              //指针
	slice  []int             //切片
	map1   map[string]string //切片
}
type Monster struct {
	Name string
	Age  int
}

func main() {
	//定义结构体变量
	var p1 Person
	fmt.Println(p1)
	if p1.ptr == nil {
		fmt.Println("ok1")
	}
	if p1.slice == nil {
		fmt.Println("ok2")
	}
	if p1.map1 == nil {
		fmt.Println("ok3")
	}
	//使用slice,一定要make
	p1.slice = make([]int, 5)
	p1.slice[0] = 100
	//map使用时同理
	p1.map1 = make(map[string]string)
	p1.map1["key1"] = "key"
	fmt.Println(p1)
	//------------
	monster := Monster{
		Name: "牛魔王",
		Age:  400,
	}
	monster1 := monster //结构体是值类型，默认为值拷贝
	monster1.Name = "红孩儿"
	fmt.Println(monster)
	fmt.Println(monster1)

	//方式3 &
	var p3 *Person = new(Person)
	//因为p3是一个指针，因此标准的给字段赋值方式
	(*p3).Name = "smith" //这种写法也可以等价于 p3.Name = "smith"
	//原因是go的设计者为了使用方便，底层会对语句进行处理，给p3加上取值运算
	(*p3).Age = 12
	fmt.Println(*p3)
	//方式4 var person *Person = &Person{}
	var p4 *Person = &Person{} //此种方法也可以直接给字符赋值
	//因为p4是一个指针，因此标准的访问字段的方法
	//(*person).Name = "scott"
	//设计者同样对底层做了处理，可以直接  person.Name = "scott"
	// 标准写法
	(*p4).Name = "scott"
	p4.Age = 16
	fmt.Println(*p4)
}
