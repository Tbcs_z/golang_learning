package main

import (
	"encoding/json"
	"fmt"
)

type Monster struct {
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Skill string `json:"skill"`
}

func main() {
	monster := Monster{"铁扇公主", 500, "芭蕉扇"}
	//将结构体变量转化为json格式字符
	//json.Marshal()函数中使用了反射
	jsonMonster, error := json.Marshal(monster)
	if error != nil {
		fmt.Println("error:", error)
	} else {
		fmt.Println(string(jsonMonster))
	}

}
