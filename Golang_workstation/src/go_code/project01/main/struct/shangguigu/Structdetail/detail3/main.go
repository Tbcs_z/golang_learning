package main

import "fmt"

type Student struct {
	Name string
	Age  int
}
type Stu Student

func main() {
	var stu1 Student
	var stu2 Stu
	//stu2 = stu1 会报错，可进行如下修改强制转换
	stu2 = Stu(stu1)
	fmt.Println(stu1, stu2)
}
