package main

import "fmt"

type Point struct {
	x int
	y int
}
type Rect struct {
	leftUp, rigthtDown Point
}
type Rect2 struct {
	leftUp, rigthtDown *Point
}

func main() {
	r1 := Rect{Point{1, 2}, Point{3, 4}}
	//r1 有4个 int，在内存中是连续分布
	//打印地址
	fmt.Printf("r1.leftUp.x 地址 = %p,r1.leftUp.y 地址 = %p\nr1.rightDown.x 地址 = %p,r1.rightDown.y 地址 = %p\n", &r1.leftUp.x, &r1.leftUp.y, &r1.rigthtDown.x, &r1.rigthtDown.y)
	//r2 有两个 *Point类型，这两个*Point类型本身地址也是连续的，它们指向的地址不一定是连续的
	r2 := Rect2{&Point{10, 20}, &Point{30, 40}}
	fmt.Printf("r2.leftUp本身地址= %p，r2.rightDown本身地址=%p\n", &r2.leftUp, &r2.rigthtDown)
	fmt.Printf("r2.leftU指向地址= %p，r2.rightDown指向地址=%p\n", r2.leftUp, r2.rigthtDown)

}
