package main

import "fmt"

type Usb interface {
	Start()
	Stop()
}
type Phone struct {
	Name string
}

func (p Phone) Start() {
	fmt.Println("手机开始工作")
}
func (p Phone) Stop() {
	fmt.Println("手机停止工作")
}
func (p Phone) Call() {
	fmt.Println("Phone is called.....")
}

type Camera struct {
	Name string
}

func (c Camera) Start() {
	fmt.Println("相机开始工作")
}
func (c Camera) Stop() {
	fmt.Println("相机停止工作")
}

type Computer struct {
}

func (c Computer) Working(usb Usb) {
	//通过Usb接口变量来调用Start和stop方法
	usb.Start()
	y, ok := usb.(Phone)
	if ok {
		y.Call()
	}
	usb.Stop()
}

type Student struct {
	Name string
}

// ...interface表示可变，可接受任意多个实参
func TypeIs(x ...interface{}) {
	for i, i2 := range x {
		switch i2.(type) {
		case string:
			fmt.Printf("#%v is String，value is %v\n", i, i2)
		case bool:
			fmt.Printf("#%v is Bool，value is %v\n", i, i2)
		case int, int64:
			fmt.Printf("#%v is Int，value is %v\n", i, i2)
		case nil:
			fmt.Printf("#%v is Nil，value is %v\n", i, i2)
		case float64:
			fmt.Printf("#%v is Float，value is %v\n", i, i2)
		case Student:
			fmt.Printf("#%v is Student，value is %v\n", i, i2)
		case *Student:
			fmt.Printf("#%v is *Student，value is %v\n", i, i2)
		default:
			fmt.Printf("#%v 's type is unknown，value is %v\n", i, i2)
		}
	}
}
func main() {
	//定义一个Usb接口数组，可以存放phone和camera的结构体变量
	var UsbArr [3]Usb //多态数组
	UsbArr[0] = Phone{"vivo"}
	UsbArr[1] = Phone{"iPhone"}
	UsbArr[2] = Camera{"Nikon"}
	fmt.Println(UsbArr)
	var computer Computer
	for _, usb := range UsbArr {
		computer.Working(usb)
	}
	var n1 float64 = 1.3
	var n2 float32 = 1.4
	var n3 int32 = 10
	var n4 int64 = 10
	var correct bool = true
	address := "我是你爹"
	student := Student{Name: "xiaoming"}
	TypeIs(n1, n2, n3, n4, correct, address, student)
}
