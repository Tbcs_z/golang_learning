package main

import "fmt"

//猴子结构体
type Monkey struct {
	Name string
}

func (m *Monkey) Climb() {
	fmt.Println(m.Name, "is climbing.....")
}

//声明接口
type SuperAbility interface {
	Flying()
	Swimming()
}

//定义一个little monkey
type LittleMonkey struct {
	Monkey
}

func (m LittleMonkey) Flying() {
	fmt.Println(m.Name, "is flying......")
}
func (m LittleMonkey) Swimming() {
	fmt.Println(m.Name, "is Swimming......")
}
func main() {
	lm := LittleMonkey{Monkey{Name: "悟空"}}
	lm.Climb()
	var SuperMonkey SuperAbility = lm
	//小猴子可以继承猴子的方法使用接口
	SuperMonkey.Flying()
	SuperMonkey.Swimming()
}
