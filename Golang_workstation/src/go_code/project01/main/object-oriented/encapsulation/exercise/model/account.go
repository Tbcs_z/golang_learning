package model

import "fmt"

type account struct {
	Id      string
	balance float64
	pwd     string
}

//account封装方法
func NewAccount(id string) *account {
	if len(id) > 5 && len(id) < 11 {
		return &account{
			Id: id,
		}
	} else {
		fmt.Println("账号长度不正确")
	}
	return nil
}
func (a *account) SetPwd(pwd string) {
	if len(pwd) != 6 {
		fmt.Println("密码长度不正确！")
	} else {
		a.pwd = pwd
		fmt.Println("密码设置成功")
	}
}
func (a *account) GetPwd() string {
	return a.pwd
}
func (a *account) SetBalance(pwd string, money float64) {
	if pwd != a.pwd {
		fmt.Println("密码不正确！")
	} else if money < 20 {
		fmt.Println("余额设置错误！")
	} else {
		a.balance = money
		fmt.Println("ok")
	}
}
func (a *account) GetBalance(pwd string) float64 {
	if pwd != a.pwd {
		fmt.Println("密码不正确！")
		return 0
	}
	return a.balance
}
