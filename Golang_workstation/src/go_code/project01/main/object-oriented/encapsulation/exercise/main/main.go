package main

import (
	"fmt"
	"go_code/project01/main/object-oriented/encapsulation/exercise/model"
)

func main() {
	acc := model.NewAccount("80082088")
	fmt.Println(*acc)
	acc.SetPwd("123456")
	acc.SetBalance("123456", 800)
	fmt.Printf("Id:%v\nPwd:%v\nBalance:%v", acc.Id, acc.GetPwd(), acc.GetBalance("123456"))
}
