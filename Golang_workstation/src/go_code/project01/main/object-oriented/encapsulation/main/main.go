package main

import (
	"fmt"
	"go_code/project01/main/object-oriented/encapsulation/model"
)

func main() {
	p := model.NewPerson("smith")
	p.SetAge(18)
	p.SetSal(5000)
	fmt.Println(*p)
	fmt.Printf("Name:%v\nAge:%v\nSalary:%v", p.Name, p.GetAge(), p.GetSal())

}
