package main

import "fmt"

type Account struct {
	No      string
	Pwd     string
	Balance float64
}

func (a *Account) Deposite(money float64, pwd string) {
	//验证密码
	if pwd != a.Pwd {
		fmt.Println("密码错误")
		return
	}
	//查看存款是否正确
	if money <= 0 {
		fmt.Println("输入金额错误！")
		return
	}
	a.Balance += money
	fmt.Println("存款成功！")
}
func (a *Account) WithDraw(money float64, pwd string) {
	//验证密码
	if pwd != a.Pwd {
		fmt.Println("密码错误")
		return
	}
	//查看取款是否正确
	if money <= 0 || money > a.Balance {
		fmt.Println("输入金额错误！")
		return
	}
	a.Balance -= money
	fmt.Println("取款成功！")
}

//查询余额
func (a *Account) Query(pwd string) {
	//验证密码
	if pwd != a.Pwd {
		fmt.Println("密码错误")
		return
	}
	fmt.Println("账户：", a.No, "\n余额：", a.Balance)
}
func main() {
	acc := Account{
		No:      "100032131",
		Pwd:     "123456",
		Balance: 8885.5,
	}
	for {
		fmt.Println("-----选择操作------")
		fmt.Println("1、查询    2、存款    3、取款")
		var choose int
		fmt.Print("请选择：")
		fmt.Scanln(&choose)
		var pwd string
		fmt.Print("输入密码：")
		fmt.Scanln(&pwd)
		switch choose {
		case 1:
			acc.Query(pwd)
		case 2:
			var money float64
			fmt.Print("\n输入存款金额：")
			fmt.Scanln(&money)
			acc.Deposite(money, pwd)
		case 3:
			var money float64
			fmt.Print("\n输入取款金额：")
			fmt.Scanln(&money)
			acc.WithDraw(money, pwd)
		default:
			fmt.Println("输入的选项错误,请重新输入")
		}
	}

}
