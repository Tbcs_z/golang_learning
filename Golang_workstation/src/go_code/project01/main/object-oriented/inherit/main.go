package main

import "fmt"

type Student struct {
	Name  string
	Age   int
	Score int
}

type Pupil struct {
	Student //嵌入了Student匿名结构体
}

//大学生
type Graduate struct {
	Student //这里就是匿名结构体
	Major   string
}

//将pupil和graduate共有的方法也绑定到*Student
func (stu *Student) ShowInfo() {
	fmt.Printf("学生名：%v，年龄：%v，成绩：%v\n", stu.Name, stu.Age, stu.Score)
}

func (stu *Student) SetScore(score int) {
	stu.Score = score
}

//这是 pupil结构体特有的方法，保留
func (p *Pupil) testing() {
	fmt.Println("小学生正在考试中......")
}
func (p *Graduate) testing() {
	fmt.Println("大学生正在考试中......")
}
func (p *Graduate) ShowInfo() {
	fmt.Printf("学生名：%v，年龄：%v，成绩：%v，专业：%v\n", p.Name, p.Age, p.Score, p.Major)

}
func main() {
	//当对结构体嵌入了匿名结构体后，使用方法会发生变化
	pupil := &Pupil{Student{
		Name:  "Tom",
		Age:   8,
		Score: 88,
	}}
	pupil.testing()
	pupil.SetScore(92)
	pupil.ShowInfo()
	graduate := &Graduate{
		Student: Student{
			Name:  "Jack",
			Age:   19,
			Score: 76,
		},
		Major: "计算机",
	}
	graduate.testing()
	graduate.SetScore(80)
	graduate.ShowInfo()
}
