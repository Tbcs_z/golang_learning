package main

import "fmt"

type A struct {
	Name string
	Age  int
}
type B struct {
	Name string
	Age  int
}
type C struct {
	A
	B
}
type D struct {
	a A //有名结构体
}

func main() {
	var c C
	//如果c没有Name字段，而A和B有Name，这时就必须通过指定匿名结构体来区分
	c.A.Name = "Tom"
	fmt.Println(c)
	var d D
	d.a.Name = "jack"
	fmt.Println(d)
}
