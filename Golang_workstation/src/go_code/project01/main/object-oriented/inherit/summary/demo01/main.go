package main

import "fmt"

type A struct {
	Name string
	age  int
}

func (a *A) Say() {
	fmt.Println("A Say OK", a.Name)
}
func (a *A) hello() {
	fmt.Println("A say Hello", a.Name)
}

type B struct {
	A
}

func (b *B) hello() {
	fmt.Println("B say Hello", b.Name)
}
func main() {
	var b B
	b.Name = "tom"
	b.Say()
	b.hello()
}
