package main

import (
	"fmt"
	"math/rand"
	"sort"
)

//hero结构体
type Hero struct {
	Name string
	Age  int
}

//声明一个Hero结构体切片类型
type HeroSlice []Hero

//实现接口 Interface
func (s HeroSlice) Len() int {
	return len(s)
}

//此方法决定使用什么标准排序
//按年龄从小到大排序
func (s HeroSlice) Less(i, j int) bool {
	//return s[i].Age < s[j].Age
	//修改成对姓名排序
	return s[i].Name < s[j].Name
}
func (s HeroSlice) Swap(i, j int) {
	//temp := s[i]
	//s[i] = s[j]
	//s[j] = temp
	//等价于上面的三行代码
	s[i], s[j] = s[j], s[i]
}

//声明student结构体
type Student struct {
	Name  string
	Age   int
	Score float64
}

//将student的切片按成绩从大到小排序
type Stus []Student

func (s Stus) Len() int {
	return len(s)
}
func (s Stus) Less(i, j int) bool {
	return s[i].Score > s[j].Score
}
func (s Stus) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func main() {
	//先定义数组或切片
	arr := []int{0, -1, 10, 7, 90}
	//要求对arr进行排序
	//1、冒泡排序
	//2、使用系统提供的方法 sort.Ints
	sort.Ints(arr)
	fmt.Println(arr)
	//对结构体切片进行排序
	var HeroS HeroSlice
	for i := 0; i < 10; i++ {
		hero := Hero{
			Name: fmt.Sprintf("英雄-%d", rand.Intn(100)),
			Age:  rand.Intn(100),
		}
		//将hero  append到切片
		HeroS = append(HeroS, hero)
	}
	//排序前
	for _, hero := range HeroS {
		fmt.Println(hero)
	}
	fmt.Println("-------")
	//调用sort.Sort
	sort.Sort(HeroS)
	for _, hero := range HeroS {
		fmt.Println(hero)
	}
	//学生
	var Students Stus
	for i := 0; i < 5; i++ {
		stus := Student{
			Name:  fmt.Sprintf("学生-%d", i),
			Age:   rand.Intn(20),
			Score: float64(rand.Intn(100) + i/2),
		}
		Students = append(Students, stus)
	}
	//排序前
	for _, i2 := range Students {
		fmt.Println(i2)
	}
	fmt.Println("--------排序后-------")
	sort.Sort(Students)
	for _, i2 := range Students {
		fmt.Println(i2)
	}
}
