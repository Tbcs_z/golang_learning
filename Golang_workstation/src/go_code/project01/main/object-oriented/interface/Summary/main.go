package main

import "fmt"

type AInterface interface {
	Say()
}
type A struct {
}

func (a A) Say() {
	fmt.Println("A is running....")
}

type interger int

func (i interger) Say() {
	fmt.Println("Interger say i=", i)
}

type BInterface interface {
	AInterface
	Hello()
}
type Monster struct {
}

func (m Monster) Hello() {
	fmt.Println("monster.HEllo")
}
func (m Monster) Say() {
	fmt.Println("MONster.say")
}
func main() {
	a := A{}
	var s AInterface = a
	s.Say()
	var i interger = 10
	var b AInterface = i
	b.Say()
	//monster是西安了 A和B接口
	var monster Monster
	var monA AInterface = monster
	var monB BInterface = monster
	monA.Say()
	monB.Hello()
}
