package main

import "fmt"

/*
1)启动一个协程，将1-2000的数放入到一个channel中，比如numChan
2)启动8个协程，从numChan:取出数（比如n),并计算1+.+n的值，并
存放到resChan
3)最后8个协程协同完成工作后，再遍历resChan,显示结果如res1=1.res10)=55
*/
func write(numChan chan int) {
	for i := 1; i <= 2000; i++ {
		numChan <- i
	}
	close(numChan)
}
func pip(numChan chan int, resChan chan int) {
	for {
		v, ok := <-numChan
		if ok {
			res := 0
			for i := 0; i <= v; i++ {
				res += i
			}
			resChan <- res
		} else {
			break
		}
	}
}
func main() {
	numChan := make(chan int)
	resChan := make(chan int, 2000)
	go write(numChan)
	for i := 0; i < 8; i++ {
		go pip(numChan, resChan)

	}
	for {
		if len(resChan) == 2000 {
			close(resChan)
			break
		}
	}
	num := 1
	for i := range resChan {
		fmt.Printf("resChan[%v] = %v\n", num, i)
		num++
	}
}
