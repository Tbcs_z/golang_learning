package main

import "fmt"

func main() {
	//管道的使用演示
	//	1、创建一个可以存放三个int类型的管道
	var intChan chan int
	intChan = make(chan int, 3)
	// 2、看看intChan是什么
	fmt.Printf("value = %v\n本身地址=%p\n", intChan, &intChan)

	//3、向管道写入数据
	intChan <- 10
	num := 211
	intChan <- num
	intChan <- 50
	//intChan <- 98//注意：当给管道写入数据时，不能超过其容量
	//4、看看管道的长度和cap（容量）
	fmt.Println("channel len =", len(intChan), " cap =", cap(intChan)) //3,3
	//5、从管道中读取数据
	num2 := <-intChan
	fmt.Println("num2 =", num2)
	fmt.Println("channel len =", len(intChan), " cap =", cap(intChan)) //2,3
	//6、在没有使用协程的情况下，如果管道的数据已经全部取出，再取就会报告 deadlock
	num3 := <-intChan
	num4 := <-intChan
	//num5 := <-intChan
	fmt.Println("num3 =", num3, "num4 =", num4)
	fmt.Println("channel len =", len(intChan), " cap =", cap(intChan)) //0,3
}
