package main

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

var wrflag = 10
var reflag = 10

func writeDataToFile(filePath string) {
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("open file err :", err)
		return
	}
	rand.Seed(time.Now().UnixNano())
	writer := bufio.NewWriter(file)
	for i := 0; i < 1000; i++ {
		str := rand.Intn(1000)
		_, err := writer.WriteString(strconv.Itoa(str) + "\n")
		if err != nil {
			return
		}
	}
	writer.Flush()
	fmt.Println("Write OK")
	wrflag--
	defer file.Close()
}

func sort(filePath, dstPath string) {
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("Open file err:", err)
	}
	file2, err1 := os.OpenFile(dstPath, os.O_RDWR|os.O_CREATE, 0666)
	if err1 != nil {
		fmt.Println("Open file err:", err)
	}
	reader := bufio.NewReader(file)
	writer := bufio.NewWriter(file2)
	var arr []int
	for {
		str, err := reader.ReadString('\n') //读到一个换行符就结束
		if err == io.EOF {                  //io.EOF表示文件的末尾
			break
		}
		str = strings.Replace(str, "\n", "", -1) //去除换行符
		num, err01 := strconv.Atoi(str)
		if err01 != nil {
			fmt.Println(err01)
			return
		}
		arr = append(arr, num)
	}
	BubbleSort(arr)
	for _, v := range arr {
		_, err := writer.WriteString(strconv.Itoa(v) + "\n")
		if err != nil {
			return
		}
	}
	writer.Flush()
	reflag--
	defer file.Close()
	defer file2.Close()
}

// BubbleSort 冒泡排序
func BubbleSort(data []int) {
	var swapped = true
	j := 0
	for swapped {
		swapped = false
		for i := 1; i < len(data)-j; i++ {
			if data[i-1] > data[i] {
				//fmt.Printf("Swapping %d, %d\n", data[i-1], data[i])
				data[i], data[i-1] = data[i-1], data[i]
				swapped = true
			}
		}
		j++
	}
}
func main() {
	/*
		1)开一个协程writeDataToFile,随机生成1000个数据，存放到文件中
		2)当writeDataToFile完成写1000个数据到文件后，让sort协程从文件中读1000个文件，并完成排序，重新写入到另外一个文件
		3)考察点：协程和管道+文件的综合使用
		4)功能扩展：开10个协程writeDataToFile,每个协程随机生成1000个数据，存放到10文件中
		5)当10个文件都生成了，让10个sort协程从10文件中读取1000个文件，并完成排序，重新写入到10个结果文件
	*/
	filePath := "D:/abc.txt"
	SortFilePath := "D:/abc_test.txt"
	//intChan := make(chan int, 1000)
	//dstChan := make(chan int, 1000)
	fmt.Println("start")
	//扩展
	for i := 1; i <= 10; i++ {
		filePath = "D:/abc" + strconv.Itoa(i) + ".txt"
		SortFilePath = "D:/abc_test" + strconv.Itoa(i) + ".txt"
		go writeDataToFile(filePath)
		go sort(filePath, SortFilePath)
	}
	for {
		if wrflag == 0 && reflag == 0 {
			break
		}
	}
}
