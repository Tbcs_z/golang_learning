package main

import (
	"fmt"
	"time"
)

type Cat struct {
	Name string
	Age  int
}

func main() {
	var allChan chan interface{}
	allChan = make(chan interface{}, 3)
	cat1 := Cat{Age: 10, Name: "Cat1"}
	allChan <- 10
	allChan <- "tom"
	allChan <- cat1
	//希望获得到管道中的第三个元素，则需先推出前两个
	<-allChan
	<-allChan
	newCat := <-allChan
	fmt.Printf("从管道中取出的猫是：%T,值为：%v\n", newCat, newCat)

	//fmt.Println("newCat.Name = ", newCat.Name)  这种写法是错误的，编译不通过
	//使用类型断言即可
	a := newCat.(Cat)
	fmt.Println(time.Now())
	fmt.Println("newCat.Name = ", a.Name)
}
