package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Person
/*
1、创建一个Person结构体[Name,Age,Address]
2、使用rand方法配合随机创建10个Person实例，并放入到channeli中.
3、遍历channel,将各个Person实例的信息显示在终端
*/
type Person struct {
	Name    string
	Age     int
	Address string
}

func main() {
	rand.Seed(time.Now().UnixNano())
	chaneli := make(chan Person, 10)
	for i := 0; i < 10; i++ {
		person := randP()
		chaneli <- person
		fmt.Println(person)
	}
	for i := 0; i < 10; i++ {
		a := <-chaneli
		fmt.Printf("%v :Name=%v,Age=%v,Address=%v\n", i+1, a.Name, a.Age, a.Address)

	}
}

func randP() Person {
	Name := RandomString(3)
	Address := RandomString(10)
	Age := rand.Intn(100)
	return Person{
		Name:    Name,
		Age:     Age,
		Address: Address,
	}
}

var defaultLetters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// RandomString returns a random string with a fixed length
func RandomString(n int, allowedChars ...[]rune) string {
	var letters []rune

	if len(allowedChars) == 0 {
		letters = defaultLetters
	} else {
		letters = allowedChars[0]
	}

	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}
