package main

import (
	"fmt"
	"sync"
	"time"
)

var (
	myMap = make(map[int]int, 10)
	//声明一个全局的互斥锁
	//lock  是一个全局的互斥锁，
	//sync  是包:synchronized 同步
	//Mutex 互斥
	lock sync.Mutex
)

//test函数就是计算 n! ,然后将这个结果放入到 mymap 中
func test(n int) {
	res := 1
	for i := 1; i <= n; i++ {
		res *= i
	}
	//这里我们将res结果放入到mymap中
	//加锁
	lock.Lock()
	myMap[n] = res
	lock.Unlock()
}

func main() {
	//需求：现在要计算1-200的各个数的阶乘，并且把各个数的阶乘放入到map中。最后显示出来。要求使用goroutine完成
	/*
		思路：
		1、编写一个函数来计算各个数的阶乘。并放入到 map 中
		2、启动的协程多个，统一访问map并将结果放入
		3、map应该做成一个全局的
	*/

	//这里开启多个协程完成这个任务
	for i := 1; i <= 20; i++ {
		go test(i)
	}
	//输出结果
	time.Sleep(10 * time.Second)
	lock.Lock()
	for i, v := range myMap {
		fmt.Printf("map[%d] = %v\n", i, v)
	}
	lock.Unlock()
}
