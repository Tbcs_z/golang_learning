package main

import "fmt"

func main() {
	intChan := make(chan int, 3)
	intChan <- 100
	intChan <- 200
	close(intChan) //close
	//此时不可再写入到channel
	//关闭后可以读取数据
	n := <-intChan
	fmt.Println(n)

	//遍历管道
	intChan2 := make(chan int, 100)
	for i := 0; i < 100; i++ {
		intChan2 <- i * 2 // 放入100个数据到intChan2
	}
	//遍历：
	//for i := 0; i < len(intChan2); i++ {
	//} 这种方式只能取出50个，每取一次会减少长度
	//管道关闭后才可以遍历，如果遍历时没有关闭，则会出现deadlock
	close(intChan2)
	for v := range intChan2 {
		fmt.Println("v =", v)
	}
}
