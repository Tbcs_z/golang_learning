package main

import "fmt"

func Fibonacil(c, quit chan int) {
	x, y := 1, 1
	for {
		select {
		case c <- x:
			//如果chan1成功读到数据，则进行case处理语句
			x, y = y, y+x
		case <-quit:
			fmt.Println("quit")
			return
			//如果chan1成功读到数据，则进行case处理语句
		}
	}

}
func main() {
	//单流程状态下一个go智能监控一个channel的状态，select可以完成监控多个channel的状态
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 6; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	Fibonacil(c, quit)

}
