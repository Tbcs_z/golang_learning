package main

import "fmt"

type Reader interface {
	ReadBook()
}
type Writer interface {
	WriteBook()
}
type Book struct {
}

func (this *Book) ReadBook() {
	fmt.Println("Read a book")
}
func (this *Book) WriteBook() {
	fmt.Println("Write a book")
}
func main() {
	//b :pair<tyoe:Book,value:book()地址>
	b := &Book{}
	//r: pair<type:,value:>
	var r Reader
	//r: pair<tyoe:Book,value:Book()地址>
	r = b
	r.ReadBook()
	var w Writer
	//r: pair<tyoe:Book,value:Book()地址>
	w = r.(Writer) //此处的断言为什么会成功？ 因为w r 具体的type是一致的
	w.WriteBook()
}
