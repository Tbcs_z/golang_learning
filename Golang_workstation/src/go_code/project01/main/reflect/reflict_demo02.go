package main

import (
	"fmt"
	"reflect"
)

type User struct {
	Id   int
	Name string
	Age  int
}

func (this User) Call() {
	fmt.Println("user is called...")
	fmt.Println("%v \n", this)
}
func main() {
	user := User{1, "A", 18}

	DeFileAndMethod(user)
}

func DeFileAndMethod(input interface{}) {

	//获取input的type
	inputType := reflect.TypeOf(input)
	fmt.Println("inputType is :", inputType.Name())
	//获取input的value
	inputValue := reflect.ValueOf(input)
	fmt.Println("inputValue is :", inputValue)

	//通过input 获取里面的字段
	/*
		1、获取interface的reflect。Type，通过type得到NumField，进行遍历
		2、得到每个field数据类型
		3、通过field有一个interface()方法得到对应的value
	*/
	for i := 0; i < inputType.NumField(); i++ {
		field := inputType.Field(i)
		value := inputValue.Field(i).Interface()

		fmt.Printf("%s : %v = %v\n", field.Name, field.Type, value)
	}
	//通过input 获取里面的方法，调用
	for x := 0; x < inputType.NumMethod(); x++ {
		m := inputType.Method(x)
		fmt.Printf("%s : %v\n", m.Name, m.Type)
	}
}
