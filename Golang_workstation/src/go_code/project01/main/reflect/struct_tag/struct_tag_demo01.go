package main

import (
	"fmt"
	"reflect"
)

//反射解析结构体标签
type resume struct {
	Name string `info:"Name" doc:"我的名字"`
	Sex  string `info:"Sex"`
}

func findTag(str interface{}) {
	t := reflect.TypeOf(str).Elem()

	for i := 0; i < t.NumField(); i++ {
		tagStr := t.Field(i).Tag.Get("info")
		tagDoc := t.Field(i).Tag.Get("doc")
		fmt.Println("info:", tagStr)
		fmt.Println("doc :", tagDoc)
	}
}
func main() {
	var re resume
	findTag(&re)
}
