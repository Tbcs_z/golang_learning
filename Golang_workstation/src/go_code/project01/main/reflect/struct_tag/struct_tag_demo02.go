package main

//结构体标签在json中的应用
import (
	"encoding/json"
	"fmt"
)

type Movie struct {
	Title string   `json:"title"`
	Year  int      `json:"year"`
	Price int      `json:"price"`
	Actor []string `json:"actors"`
}

func main() {
	movie := Movie{"泰囧", 2012, 10, []string{"王宝强", "徐峥"}}
	//编码的过程 结构体 ——> json
	jsonstr, err := json.Marshal(movie)
	if err != nil {
		fmt.Println("json marshal error", err)
		return
	}
	fmt.Printf("jsonStr = %s \n", jsonstr)

	//解码的过程 jsonstr --> 结构体
	my_movie := Movie{}
	err = json.Unmarshal(jsonstr, &my_movie)
	if err != nil {
		fmt.Println("json Unmarshal error", err)
		return
	}
	fmt.Printf("my_movie: %v", my_movie)
}
