package main

import (
	"encoding/json"
	"fmt"
)

//反序列化
//定义一个结构体
type Monster struct {
	Name     string  `json:"name"` //反射机制
	Age      int     `json:"age"`
	Birthday string  `json:"birthday"`
	Sal      float64 `json:"sal"`
	Skill    string  `json:"skill"`
}

func testStruct() []byte {
	//演示对结构体的序列化操作
	monster := Monster{
		Name:     "牛魔王",
		Age:      500,
		Birthday: "1600.1.1",
		Sal:      4000,
		Skill:    "喷火",
	}
	//将monster 序列化
	data, err := json.Marshal(monster)
	if err != nil {
		fmt.Println("err:", err)
	}
	//输出序列化后的结果
	//fmt.Println(string(data))
	return data
}

//演示将json字符串，反序列化成struct
func unmarshallStruct() {
	//str在真实的项目开发中是通过网络传输获取的，或者是读取文件获取
	str := "{\"Name\":\"牛魔王\",\"Age\":500,\"Birthday\":\"2011-11-11\",\"Sal\":8000,\"Skill\":\"牛魔拳\"}"
	var data Monster
	err := json.Unmarshal([]byte(str), &data)
	if err != nil {
		fmt.Println("unmarshal fail,err:", err)
	}
	fmt.Println("反序列化后，Monster = ", data)
}
func UnM() {
	var monster Monster
	data := testStruct()
	err := json.Unmarshal([]byte(data), &monster)
	if err != nil {
		fmt.Println("unmarshal fail,err:", err)
	}
	fmt.Println(monster)
}
func unMarshalMap() {
	str := "{\"address\":\"火焰山\",\"age\":30,\"name\":\"红孩儿\"}"
	//定义一个map
	var data map[string]interface{}
	//注意：反序列化map时不需要make，因为make操作被封装到 Unmarshal函数里了
	//反序列化
	err := json.Unmarshal([]byte(str), &data)
	if err != nil {
		fmt.Println("unmarshal fail,err:", err)
	}
	fmt.Println(data)
}

//反序列化成slice
func unMarshalSlice() {
	str := "[{\"Address\":\"四川\",\"Age\"" +
		":19,\"Name\":\"Jack\"},{\"" +
		"Address\":[\"重庆\",\"墨西哥\"],\"Age\":29,\"Name\":\"Tom\"}]\n"
	//定义一个切片
	var slice []map[string]interface{}
	err := json.Unmarshal([]byte(str), &slice)
	if err != nil {
		fmt.Println("unmarshal fail,err:", err)
	}
	fmt.Println(slice)
}
func main() {
	unmarshallStruct()
	UnM()
	unMarshalMap()
	unMarshalSlice()
}
