package main

import (
	"encoding/json"
	"fmt"
)

//定义一个结构体
type Monster struct {
	Name     string  `json:"name"` //反射机制
	Age      int     `json:"age"`
	Birthday string  `json:"birthday"`
	Sal      float64 `json:"sal"`
	Skill    string  `json:"skill"`
}

func testStruct() {
	//演示对结构体的序列化操作
	monster := Monster{
		Name:     "牛魔王",
		Age:      500,
		Birthday: "1600.1.1",
		Sal:      4000,
		Skill:    "喷火",
	}
	//将monster 序列化
	data, err := json.Marshal(monster)
	if err != nil {
		fmt.Println("err:", err)
	}
	//输出序列化后的结果
	fmt.Println(string(data))
}
func testMap() {
	//定义一个mapo
	var a map[string]interface{}
	//使用map，需要make
	a = make(map[string]interface{})
	a["name"] = "红孩儿"
	a["age"] = 30
	a["address"] = "火焰山"
	data, err := json.Marshal(a)
	if err != nil {
		fmt.Println("err:", err)
	}
	//输出序列化后的结果
	fmt.Println(string(data))
}
func testSlice() {
	//定义一个
	var slice []map[string]interface{}
	var m1 map[string]interface{}
	//使用map，需要make
	m1 = make(map[string]interface{})
	m1["Name"] = "Jack"
	m1["Age"] = 19
	m1["Address"] = "四川"
	slice = append(slice, m1)
	var m2 map[string]interface{}
	m2 = make(map[string]interface{})
	m2["Name"] = "Tom"
	m2["Age"] = 29
	m2["Address"] = [2]string{"重庆", "墨西哥"}
	slice = append(slice, m2)
	data, err := json.Marshal(slice)
	if err != nil {
		fmt.Println("err:", err)
	}
	//输出序列化后的结果
	fmt.Println(string(data))
}

//对基本数据类型序列化
func testFloat64() {
	var num1 float64 = 2345.67
	data, err := json.Marshal(num1)
	if err != nil {
		fmt.Println("err:", err)
	}
	//输出序列化后的结果
	fmt.Println(string(data))
}
func main() {
	//演示将结构体，map，切片进行序列化
	testStruct()
	testMap()
	testSlice()
	testFloat64()
}
