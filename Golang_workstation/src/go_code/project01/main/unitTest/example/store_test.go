package main

import "testing"

func TestMonster_Store(t *testing.T) {
	var monster = &Monster{
		Name:  "牛魔王",
		Age:   400,
		Skill: "喷火",
	}
	data := monster.Store()
	if !data {
		t.Fatalf("Error!Unexpected result!")
	}
	t.Logf("Function testing OK!")
}
func TestMonster_ReStore(t *testing.T) {
	var monster = &Monster{}
	data := monster.ReStore()
	if !data {
		t.Fatalf("Error!Unexpected result:%v", data)
	}
	//进一步判断
	if monster.Name != "牛魔王" {
		t.Fatalf("Error!Expected value = %v,Unexpected result:%v", "牛魔王", monster.Name)
	}
	t.Logf("Function testing OK!")
}
