package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Monster struct {
	Name  string
	Age   int
	Skill string
}

func (m *Monster) Store() bool {
	//序列化
	data, err := json.Marshal(m)
	if err != nil {
		fmt.Println("Marshal error:", err)
	}
	err01 := ioutil.WriteFile("D://monster.txt", data, 0666)
	if err01 != nil {
		fmt.Println("err:", err01)
		return false
	}
	fmt.Println(string(data))
	return true
}

func (m *Monster) ReStore() bool {
	data, err01 := ioutil.ReadFile("D://monster.txt")
	if err01 != nil {
		fmt.Println("err:", err01)
		return false
	}
	err := json.Unmarshal(data, m)
	if err != nil {
		fmt.Println("Marshal error:", err)
		return false
	}
	fmt.Println(*m)
	return true
}
