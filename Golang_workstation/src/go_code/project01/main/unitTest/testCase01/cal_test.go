package main

import (
	"fmt"
	"testing"
) //引入go 的testing框架包

//编写一个测试用例，去测试写的函数是否正确

func TestAddUpper(t *testing.T) {
	//调用
	res := AddUpper(10)
	if res != 55 {
		//fmt.Printf("AddUpper(10) error,expected value=%v,but output value is %v\n", 55, res)
		t.Fatalf("Addupper(10) error,expected value=%v,but output value is %v\n", 55, res)
	}
	//如果正确，就输出日志
	t.Logf("AddUpper 执行正确...")
}
func TestHello(t *testing.T) {
	fmt.Println("TestHello 被调用")
}
