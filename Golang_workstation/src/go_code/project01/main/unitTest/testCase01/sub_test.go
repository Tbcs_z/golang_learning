package main

import (
	"testing"
) //引入go 的testing框架包

//编写一个测试用例，去测试写的函数是否正确

func TestSub(t *testing.T) {
	//调用
	res := Sub(10, 6)
	if res != 4 {
		//fmt.Printf("Sub(10,6) error,expected value=%v,but output value is %v\n", 55, res)
		t.Fatalf("Sub(10,6) error,expected value=%v,but output value is %v\n", 55, res)
	}
	//如果正确，就输出日志
	t.Logf("Sub() 执行正确...")
}
