package main

import "fmt"

func addUpper(n int) int {
	res := 0
	for i := 1; i <= n; i++ {
		res += i
	}
	return res
}

func main() {
	//传统测试方法就是再main函数中使用看结果是否正确
	res := addUpper(10)
	if res != 55 {
		fmt.Println("addUpper Error: unexpected values")
	} else {
		fmt.Println("Ok!")
	}
}
