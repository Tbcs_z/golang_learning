package main

import "fmt"

func changeValue(p int) {
	p = 10
}

/*
	go与C不同，
	C : int* a
	go: a *int
*/
func changeIndex(p *int) {
	*p = 10
}
func swap(a, b *int) {
	var temp int
	temp = *a
	*a = *b
	*b = temp

}
func main() {
	var a int = 1
	changeValue(a)
	fmt.Println("a =", a)
	changeIndex(&a)
	fmt.Println("a =", a)

	//通过指针交换两个变量的值
	var x, y int = 10, 20
	fmt.Println("x = ", x, " y = ", y)
	swap(&x, &y)
	fmt.Println("x = ", x, " y = ", y)

	var p *int
	p = &a
	fmt.Println(&a)
	fmt.Println(p)
	//二级指针
	var pp **int
	pp = &p
	fmt.Println(&p)
	fmt.Println(pp)
}
